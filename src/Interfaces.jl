
"""
    call_NE3DLE(file::String)
    call_NE3DLE(par_dict::AbstractDict)
    call_NE3DLE(surface::VmecSurface{T}; M_θ::Int=32, N_ζ::Int=32, α_0::T=0.0, nz0::Int=128, n_pol::Int=1) where {T}

Interface function to call the local 3D equilibrium code from an input file.
Results are output in PEST coordinates for either a fieldline or a flux surface.
"""
function call_NE3DLE(file::String)
    par_dict = read_par_file(file)
    return call_NE3DLE(par_dict)
end

function call_NE3DLE(par_dict::AbstractDict)

    M_theta = par_dict["M_theta"]
    N_zeta = par_dict["N_zeta"]
    mapping = par_dict["mapping"]
    
    #These if-statements check if a VMEC surface is being recreated. 
    #Modifying a VMEC surface requires a fully populated par_dict and is carried out in the rest of the code (see get_shape() in Shaping.jl)
    if mapping == "vmec" 
        #Need to figure out how to find number of keys of a fully-populated VMEC par_dict
        if 21 > length(keys(par_dict))
            vmec_surface = initialize_from_eq!(par_dict, par_dict["vmec_file"])
            return call_NE3DLE(vmec_surface, M_θ = M_theta, N_ζ = N_zeta, α_0 = par_dict["alpha_0"], 
                               nz0 = par_dict["nz0"], n_pol = par_dict["n_pol"], 
                               fl_coordinate = par_dict["fl_coordinate"],
                               Rmn = par_dict["Rmn"], Zmn = par_dict["Zmn"])
        end
    end

    nfp = par_dict["N_field_periods"]

    shape = get_shape(par_dict)

    R_map, Z_map, Phi_map = shaping(shape)

    coords = PestGrid(M_theta,N_zeta,nfp)

    imap = coord_param(coords,shape,R_map,Z_map,Phi_map)

    if !isempty(keys(par_dict["Rmn"])) || !isempty(keys(par_dict["Zmn"]))
        update_inverse_mapping_fourier_modes!(coords,imap,par_dict["Rmn"],par_dict["Zmn"])
    end

    norms = mapping == "miller" ? NormalizingParams(par_dict,shape,imap,coords) : NormalizingParams(par_dict,imap,coords)

    metric = Metric(coords,imap)

    surface_quant = compute_input_surface_quantitites(par_dict,norms)

    computed_field_periods = haskey(par_dict,"n_pol") ? par_dict["n_pol"]*nfp/par_dict["iota"] : par_dict["computed_field_periods"]

    params = RuntimeParams(M_theta,N_zeta,par_dict["iota"],par_dict["nz0"],computed_field_periods,par_dict["alpha_0"],surface_quant,mapping,false,par_dict["fl_coordinate"],nfp)

    H, b, indices = compute_convolution_arrays(params,coords,metric)
    compute_jacobian!(H,b,indices,coords,metric)

    if sum(metric.sqrtg .<= 0) != 0 
        @warn "Unphysical equilibrium: jacobian changes signs."
        return nothing
    end
    
    geom = compute_geometry(params, coords, imap, metric, norms)

    return NE3DLESurface(coords, params, norms, imap, metric, geom)
end

"""
    NE3DLE_exec()

Exectute function for isolated use of NE3DLE. Reads NE3DLE.in, checks for
parameter scan. Then searches for output directory, and makes new output
directory if specified one exists. Then calls call_NE3DLE().
"""

function NE3DLE_exec()
    input_file = "NE3DLE.in"
    input_path = string(pwd())
    input_location = string("$input_path/","$input_file")
    par_dict = read_par_file(input_location)
    out_params = OutputParams()
    out_params.parameter_output_path = typeof(par_dict["out_path"]) == String ? [par_dict["out_path"]] : par_dict["out_path"]
    out_params.parameter_output_name = par_dict["out_name"]

    #Check how many and which parameters are scanned
    n_scan_parameters, out_params.n_scan_runs, out_params.scan_var = check_parameter_scan(par_dict,out_params)

    #Write parameter files and make outout directories
    if n_scan_parameters == 1
        parameter_output_path = Vector{String}(undef,out_params.n_scan_runs)
        out_params.parameter_output_path = write_param_file_scan(par_dict, out_params)
    else
        out_params.parameter_output_path[1] = string("$(out_params.parameter_output_path[1])","$(out_params.parameter_output_name)")
        out_params.parameter_output_path[1] = check_directory_exist!(out_params.parameter_output_path[1])
        write_param_file(input_file, input_location, out_params)
    end

    #Reinitialize TypesDict, then call NE3DLE to each input file in output directory,
    # and write output file to output directory
    for y = 1:out_params.n_scan_runs
        out_path = string("$(out_params.parameter_output_path[y])","/")
        input_location = string(out_path,input_file)
        TypesDict = Dict{String,Type}()
        surface = call_NE3DLE(input_location)
        field_line = NE3DLEFieldLine(surface)
        write_NE3DLE_out_file(out_params,field_line.params, field_line.gene, out_path)
    end
end

#Currently assumes an even number of field-line points
#Using NormalizingParams is necessary for Miller normalizations
#Not true for non-axisymmetric configs
"""
         extend_flux_tube(max_theta::Float64,
                          gene::GeneGeom,
                          norms::NormalizingParams)

Exends values in GeneGeom to a flux tube of length 2max_theta. NormalizingParams
are passed explicitly---necessary for Miller normalizations.
"""
function extend_flux_tube(max_theta::Float64,
                          gene::GeneGeom{T},
                          norms::NormalizingParams{T}) where {T}
    shat = gene.shat
    n_pol = gene.n_pol
    nz0 = gene.gridpoints

    n_points = iseven(Int(floor(max_theta*nz0/n_pol))) ? Int(floor(max_theta*nz0/n_pol)) : Int(ceil(max_theta*nz0/n_pol))
    n_intervals = Int(floor(div(max_theta-n_pol,2*n_pol)))
    n_padding_points = div(n_points-(2*n_intervals+1)*nz0,2)
    dz = 2*n_pol/nz0
    zero_index = Int(n_points/2+1)

    theta_k_ext = Vector{Float64}(undef,n_points)
    gxx_ext = Vector{Float64}(undef,n_points)
    gxy_ext = Vector{Float64}(undef,n_points)
    gyy_ext = Vector{Float64}(undef,n_points)
    Bhat_ext = Vector{Float64}(undef,n_points)
    jac_ext = Vector{Float64}(undef,n_points)
    L2_ext = Vector{Float64}(undef,n_points)
    L1_ext = Vector{Float64}(undef,n_points)
    ∂B∂θ_ext = Vector{Float64}(undef,n_points)

    gxx = gene.gxx
    gxy = gene.gxy
    gyy = gene.gyy
    Bhat = gene.Bhat
    jac = gene.jac
    L2 = gene.L2
    L1 = gene.L1
    ∂B∂θ = gene.∂B∂θ

    theta_k = -2*n_pol*(n_intervals+1)
    for i = nz0-n_padding_points+1:nz0
        k = Int(zero_index + i-1 - nz0/2 + theta_k*nz0/(2*n_pol))
        theta_k_ext[k] = (i-1 - nz0/2)*dz + theta_k
        gxx_ext[k] = gxx[i]
        gxy_ext[k] = gxy[i] + shat*theta_k*pi/(norms.ρ*norms.B_ref/norms.dpsi_drho)*gxx[i]
        gyy_ext[k] = gyy[i] + 2*shat*theta_k*pi/(norms.ρ*norms.B_ref/norms.dpsi_drho)*gxy[i] + (shat*theta_k*pi/(norms.ρ*norms.B_ref/norms.dpsi_drho))^2*gxx[i]
        Bhat_ext[k] = Bhat[i]
        jac_ext[k] = jac[i]
        L2_ext[k] = L2[i] - shat*theta_k*pi/(norms.ρ*norms.B_ref/norms.dpsi_drho)*L1[i]
        L1_ext[k] = L1[i]
        ∂B∂θ_ext[k] = ∂B∂θ[i]
    end

    for j in [n_intervals:-1:1;]
        theta_k = -2*n_pol*j
        for i = 1:nz0
            k = Int(zero_index + i-1 - nz0/2 + theta_k*nz0/(2*n_pol))
            theta_k_ext[k] = (i-1 - nz0/2)*dz + theta_k
            gxx_ext[k] = gxx[i]
            gxy_ext[k] = gxy[i] + shat*theta_k*pi/(norms.ρ*norms.B_ref/norms.dpsi_drho)*gxx[i]
            gyy_ext[k] = gyy[i] + 2*shat*theta_k*pi/(norms.ρ*norms.B_ref/norms.dpsi_drho)*gxy[i] + (shat*theta_k*pi/(norms.ρ*norms.B_ref/norms.dpsi_drho))^2*gxx[i]
            Bhat_ext[k] = Bhat[i]
            jac_ext[k] = jac[i]
            L2_ext[k] = L2[i] - shat*theta_k*pi/(norms.ρ*norms.B_ref/norms.dpsi_drho)*L1[i]
            L1_ext[k] = L1[i]
            ∂B∂θ_ext[k] = ∂B∂θ[i]
        end
    end

    for i = 1:nz0
        k = Int(zero_index + i-1 - nz0/2)
        theta_k_ext[k] = (i-1 - nz0/2)*dz
        gxx_ext[k] = gxx[i]
        gxy_ext[k] = gxy[i]
        gyy_ext[k] = gyy[i]
        Bhat_ext[k] = Bhat[i]
        jac_ext[k] = jac[i]
        L2_ext[k] = L2[i]
        L1_ext[k] = L1[i]
        ∂B∂θ_ext[k] = ∂B∂θ[i]
    end

    for j = 1:n_intervals
        theta_k = 2*n_pol*j
        for i = 1:nz0
            k = Int(zero_index + i-1 - nz0/2 + theta_k*nz0/(2*n_pol))
            theta_k_ext[k] = (i-1 - nz0/2)*dz + theta_k
            gxx_ext[k] = gxx[i]
            gxy_ext[k] = gxy[i] + shat*theta_k*pi/(norms.ρ*norms.B_ref/norms.dpsi_drho)*gxx[i]
            gyy_ext[k] = gyy[i] + 2*shat*theta_k*pi/(norms.ρ*norms.B_ref/norms.dpsi_drho)*gxy[i] + (shat*theta_k*pi/(norms.ρ*norms.B_ref/norms.dpsi_drho))^2*gxx[i]
            Bhat_ext[k] = Bhat[i]
            jac_ext[k] = jac[i]
            L2_ext[k] = L2[i] - shat*theta_k*pi/(norms.ρ*norms.B_ref/norms.dpsi_drho)*L1[i]
            L1_ext[k] = L1[i]
            ∂B∂θ_ext[k] = ∂B∂θ[i]
        end
    end

    theta_k = 2*n_pol*(n_intervals+1)
    for i = 1:n_padding_points
        k = Int(zero_index + i-1 - nz0/2 + theta_k*nz0/(2*n_pol))
        theta_k_ext[k] = (i-1 - nz0/2)*dz + theta_k
        gxx_ext[k] = gxx[i]
        gxy_ext[k] = gxy[i] + shat*theta_k*pi/(norms.ρ*norms.B_ref/norms.dpsi_drho)*gxx[i]
        gyy_ext[k] = gyy[i] + 2*shat*theta_k*pi/(norms.ρ*norms.B_ref/norms.dpsi_drho)*gxy[i] + (shat*theta_k*pi/(norms.ρ*norms.B_ref/norms.dpsi_drho))^2*gxx[i]
        Bhat_ext[k] = Bhat[i]
        jac_ext[k] = jac[i]
        L2_ext[k] = L2[i] - shat*theta_k*pi/(norms.ρ*norms.B_ref/norms.dpsi_drho)*L1[i]
        L1_ext[k] = L1[i]
        ∂B∂θ_ext[k] = ∂B∂θ[i]
    end

    return GeneGeom{T}(gene.s0,gene.iota,shat,gene.alpha_0,gene.major_R,gene.minor_a,gene.B_ref,gene.my_dpdx,n_points,n_intervals,gene.nfp,gene.Cy,gxx_ext,gxy_ext,gyy_ext,Bhat_ext,jac_ext,L2_ext,L1_ext,∂B∂θ_ext,theta_k_ext)
end

"""
         extend_flux_tube(max_theta::Float64,
                          gene::GeneGeom)

Exends values in GeneGeom to a flux tube of length 2max_theta. Leaner version
without normalizing params, i.e. assumes VMEC normalizations. Sufficient for
helical_axis mapping.
"""
function extend_flux_tube(max_theta::Float64,
                          gene::GeneGeom{T}) where {T}
    shat = gene.shat
    n_pol = gene.n_pol
    nz0 = gene.gridpoints

    n_points = iseven(Int(floor(max_theta*nz0/n_pol))) ? Int(floor(max_theta*nz0/n_pol)) : Int(ceil(max_theta*nz0/n_pol))
    n_intervals = Int(floor(div(max_theta-n_pol,2*n_pol)))
    n_padding_points = div(n_points-(2*n_intervals+1)*nz0,2)
    dz = 2*n_pol/nz0
    zero_index = Int(n_points/2+1)

    theta_k_ext = Vector{Float64}(undef,n_points)
    gxx_ext = Vector{Float64}(undef,n_points)
    gxy_ext = Vector{Float64}(undef,n_points)
    gyy_ext = Vector{Float64}(undef,n_points)
    Bhat_ext = Vector{Float64}(undef,n_points)
    jac_ext = Vector{Float64}(undef,n_points)
    L2_ext = Vector{Float64}(undef,n_points)
    L1_ext = Vector{Float64}(undef,n_points)
    ∂B∂θ_ext = Vector{Float64}(undef,n_points)

    gxx = gene.gxx
    gxy = gene.gxy
    gyy = gene.gyy
    Bhat = gene.Bhat
    jac = gene.jac
    L2 = gene.L2
    L1 = gene.L1
    ∂B∂θ = gene.∂B∂θ

    theta_k = -2*n_pol*(n_intervals+1)
    for i = nz0-n_padding_points+1:nz0
        k = Int(zero_index + i-1 - nz0/2 + theta_k*nz0/(2*n_pol))
        theta_k_ext[k] = (i-1 - nz0/2)*dz + theta_k
        gxx_ext[k] = gxx[i]
        gxy_ext[k] = gxy[i] + shat*theta_k*pi*gxx[i]
        gyy_ext[k] = gyy[i] + 2*shat*theta_k*pi*gxy[i] + (shat*theta_k*pi)^2*gxx[i]
        Bhat_ext[k] = Bhat[i]
        jac_ext[k] = jac[i]
        L2_ext[k] = L2[i] - shat*theta_k*pi*L1[i]
        L1_ext[k] = L1[i]
        ∂B∂θ_ext[k] = ∂B∂θ[i]
    end

    for j in [n_intervals:-1:1;]
        theta_k = -2*n_pol*j
        for i = 1:nz0
            k = Int(zero_index + i-1 - nz0/2 + theta_k*nz0/(2*n_pol))
            theta_k_ext[k] = (i-1 - nz0/2)*dz + theta_k
            gxx_ext[k] = gxx[i]
            gxy_ext[k] = gxy[i] + shat*theta_k*pi*gxx[i]
            gyy_ext[k] = gyy[i] + 2*shat*theta_k*pi*gxy[i] + (shat*theta_k*pi)^2*gxx[i]
            Bhat_ext[k] = Bhat[i]
            jac_ext[k] = jac[i]
            L2_ext[k] = L2[i] - shat*theta_k*pi*L1[i]
            L1_ext[k] = L1[i]
            ∂B∂θ_ext[k] = ∂B∂θ[i]
        end
    end

    for i = 1:nz0
        k = Int(zero_index + i-1 - nz0/2)
        theta_k_ext[k] = (i-1 - nz0/2)*dz
        gxx_ext[k] = gxx[i]
        gxy_ext[k] = gxy[i]
        gyy_ext[k] = gyy[i]
        Bhat_ext[k] = Bhat[i]
        jac_ext[k] = jac[i]
        L2_ext[k] = L2[i]
        L1_ext[k] = L1[i]
        ∂B∂θ_ext[k] = ∂B∂θ[i]
    end

    for j = 1:n_intervals
        theta_k = 2*n_pol*j
        for i = 1:nz0
            k = Int(zero_index + i-1 - nz0/2 + theta_k*nz0/(2*n_pol))
            theta_k_ext[k] = (i-1 - nz0/2)*dz + theta_k
            gxx_ext[k] = gxx[i]
            gxy_ext[k] = gxy[i] + shat*theta_k*pi*gxx[i]
            gyy_ext[k] = gyy[i] + 2*shat*theta_k*pi*gxy[i] + (shat*theta_k*pi)^2*gxx[i]
            Bhat_ext[k] = Bhat[i]
            jac_ext[k] = jac[i]
            L2_ext[k] = L2[i] - shat*theta_k*pi*L1[i]
            L1_ext[k] = L1[i]
            ∂B∂θ_ext[k] = ∂B∂θ[i]
        end
    end

    theta_k = 2*n_pol*(n_intervals+1)
    for i = 1:n_padding_points
        k = Int(zero_index + i-1 - nz0/2 + theta_k*nz0/(2*n_pol))
        theta_k_ext[k] = (i-1 - nz0/2)*dz + theta_k
        gxx_ext[k] = gxx[i]
        gxy_ext[k] = gxy[i] + shat*theta_k*pi*gxx[i]
        gyy_ext[k] = gyy[i] + 2*shat*theta_k*pi*gxy[i] + (shat*theta_k*pi)^2*gxx[i]
        Bhat_ext[k] = Bhat[i]
        jac_ext[k] = jac[i]
        L2_ext[k] = L2[i] - shat*theta_k*pi*L1[i]
        L1_ext[k] = L1[i]
        ∂B∂θ_ext[k] = ∂B∂θ[i]
    end

    return GeneGeom{T}(gene.s0,gene.iota,shat,gene.alpha_0,gene.major_R,gene.minor_a,gene.B_ref,gene.my_dpdx,n_points,n_intervals,gene.nfp,gene.Cy,gxx_ext,gxy_ext,gyy_ext,Bhat_ext,jac_ext,L2_ext,L1_ext,∂B∂θ_ext,theta_k_ext)
end
