function PlasmaEquilibriumToolkit.grad_B(x::C,
                                         surface::NE3DLESurface{T};
                                        ) where {T, C <: AbstractMagneticCoordinates}
    throw(ArgumentError("∇B not implemented for $C with a NE3DLESurface"))
end

function PlasmaEquilibriumToolkit.grad_B(x::FluxCoordinates,
                                         surface::NE3DLESurface{T};
                                        ) where {T}
    e_x = basis_vectors(Covariant(), CartesianFromFlux(), x, surface)
    return grad_B(x,e_x,surface)
end

function PlasmaEquilibriumToolkit.grad_B(x::PestCoordinates,
                                         surface::NE3DLESurface{T};
                                        ) where {T}
    y = FluxFromPest()(x, surface)
    return grad_B(y,surface)
end

function PlasmaEquilibriumToolkit.grad_B(e_x::BasisVectors{T},
                                         surface::NE3DLESurface{T};
                                        ) where {T}
    dBdθ = fourier_derivative(surface.coords,surface.geom.B_mag,:dθ)
    dBdζ = fourier_derivative(surface.coords,surface.geom.B_mag,:dζ)
    dBdψ = surface.geom.B_mag_psi
    return e_x[:,1]*dBdψ .+ e_x[:,2]*dBdθ .+ e_x[:,3]*dBdζ
end

function PlasmaEquilibriumToolkit.grad_B(x::FluxCoordinates,
                                         e_x::BasisVectors{T},
                                         surface::NE3DLESurface{T};
                                        ) where {T}
    dBdθ = fourier_flux_interpolation(x,surface,fourier_derivative(surface.coords,surface.geom.B_mag,:dθ))
    dBdζ = fourier_flux_interpolation(x,surface,fourier_derivative(surface.coords,surface.geom.B_mag,:dζ))
    dBdψ = fourier_flux_interpolation(x,surface,surface.geom.B_mag_psi)
    return e_x[:,1]*dBdψ .+ e_x[:,2]*dBdθ .+ e_x[:,3]*dBdζ
end

function PlasmaEquilibriumToolkit.grad_B(x::PestCoordinates,
                                         e_x::BasisVectors{T},
                                         surface::NE3DLESurface{T};
                                        ) where {T}
    y = FluxFromPest()(x, surface)
    return grad_B(y, e_x, surface)
end

function grad_jac(e_x::BasisVectors{T},
                  surface::NE3DLESurface{T};
                 ) where {T}
    dsqrtg_dθ = fourier_derivative(surface.coords,surface.metric.sqrtg,:dθ)
    dsqrtg_dζ = fourier_derivative(surface.coords,surface.metric.sqrtg,:dζ)
    dsqrtg_dψ = surface.metric.sqrtg_psi
    return e_x[:,1]*dsqrtg_dψ .+ e_x[:,2]*dsqrtg_dθ .+ e_x[:,3]*dsqrtg_dζ
end

function grad_jac(x::FluxCoordinates,
                  e_x::BasisVectors{T},
                  surface::NE3DLESurface{T};
                 ) where {T}
    dsqrtg_dθ = fourier_flux_interpolation(x,surface,fourier_derivative(surface.coords,surface.metric.sqrtg,:dθ))
    dsqrtg_dζ = fourier_flux_interpolation(x,surface,fourier_derivative(surface.coords,surface.metric.sqrtg,:dζ))
    dsqrtg_dψ = fourier_flux_interpolation(x,surface,surface.metric.sqrtg_psi)
    return e_x[:,1]*dsqrtg_dψ .+ e_x[:,2]*dsqrtg_dθ .+ e_x[:,3]*dsqrtg_dζ
end

function PlasmaEquilibriumToolkit.B_norm(surface::NE3DLESurface{T};) where {T}
    return surface.geom.B_mag
end

function PlasmaEquilibriumToolkit.B_norm(x::FluxCoordinates,
                                         surface::NE3DLESurface{T};
                                        ) where {T}
    return fourier_flux_interpolation_NE3DLE(x,surface,surface.geom.B_mag)
end

function PlasmaEquilibriumToolkit.B_norm(x::PestCoordinates,
                                         surface::NE3DLESurface{T};
                                        ) where {T}
    y = FluxFromPest()(x, surface)
    return B_norm(y,surface)
end
#=
function PlasmaEquilibriumToolkit.B_field(surface::NE3DLESurface{T};) where {T}
    ∇x = basis_vectors(Contravariant(), CartesianFromNE3DLE(), x, surface)
    return B_field(Contravariant(), ∇x)
end
=#
function PlasmaEquilibriumToolkit.B_field(x::FluxCoordinates,
                                          surface::NE3DLESurface{T};
                                         ) where {T}
    y = PestFromFlux()(x, surface)
    ∇y = basis_vectors(Contravariant(), CartesianFromPest(), y, surface)
    return B_field(Contravariant(), ∇y)
end

function PlasmaEquilibriumToolkit.B_field(x::PestCoordinates,
                                          surface::NE3DLESurface{T};
                                         ) where {T}
    ∇x = basis_vectors(Contravariant(), CartesianFromPest(), x, surface)
    return B_field(Contravariant(), ∇x)
end

function PlasmaEquilibriumToolkit.jacobian(surface::NE3DLESurface{T};) where {T}
    return surface.metric.sqrtg
end

function PlasmaEquilibriumToolkit.jacobian(x::FluxCoordinates,
                                           surface::NE3DLESurface{T};
                                          ) where {T}
    return fourier_flux_interpolation_NE3DLE(x,surface,surface.metric.sqrtg)
end

function PlasmaEquilibriumToolkit.jacobian(x::PestCoordinates,
                                           surface::NE3DLESurface{T};
                                          ) where {T}
    y = FluxFromPest()(x, surface)
    return jacobian(y, surface)
end