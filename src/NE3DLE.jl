module NE3DLE
    using CoordinateTransformations
    using DelimitedFiles
    using FFTW
    using HDF5
    using Interpolations
    using LinearAlgebra
    using OrderedCollections
    using PlasmaEquilibriumToolkit
    using Primes
    using Printf
    using QuadGK
    using Requires
    using Roots
    using SharedArrays
    using StaticArrays

    export NE3DLE_exec, call_NE3DLE, NE3DLESurface, NE3DLEFieldLine
    export write_NE3DLE_out_file, write_input_and_surface_to_hdf5_file
    export read_input_and_surface_from_hdf5_file, read_par_file

    include("TypeDefs.jl")
    include("FFT.jl")
    include("IO.jl")
    include("Shaping.jl")
    include("CoordParam.jl")
    include("Parallel.jl")
    include("InverseMapping.jl")
    include("Jacobian.jl")
    include("Geometry.jl")
    include("Interfaces.jl")
    include("FieldLine.jl")
    include("NE3DLEUtils.jl")
    include("Transformations.jl")
    include("PlasmaEquilibriumToolkitExtensions.jl")

    function __init__()
        @require StellaratorOptimization="f2f44f23-99b2-4d7c-931e-31858dc08d61" begin
            include("optimization/StellOptInterface.jl")
            include("optimization/OptimizationTargets.jl")
            include("optimization/OptimizationUtils.jl")
        end
        @require VMEC="2b46c670-0004-47b5-bf0a-1741584931e9" include("VmecInterface.jl")
    end
end
