import PlasmaEquilibriumToolkit.basis_vectors, PlasmaEquilibriumToolkit.transform_basis

struct BoozerFromNE3DLE <: Transformation; end
struct CylindricalFromNE3DLE <: Transformation; end
struct FluxFromNE3DLE <: Transformation; end
struct NE3DLEFromFlux <: Transformation; end
struct PestFromNE3DLE <: Transformation; end
struct CartesianFromNE3DLE <: Transformation; end
struct Derivatives end

#Coordinate transformation functions to help integrate with PlasmaEquilibriumToolkit
"""
Note, PEST coordinates are (ψ,θ,ζ) and Straight Field Line (SFL) coordinates  are 
(ψ,α=θ-ιζ,ζ). However, PlasmaEquilibriumToolkit.jl calls PEST coordinates Flux 
coordinates and SFL coordinates Pest coordinates. NE3DLE uses PEST/Flux coordinates
to compute quantities on a single field period of a flux surface.
"""
#NOT POINT-WISE
function (::FluxFromNE3DLE)(surface::NE3DLESurface{T}) where {T}
    coords = surface.coords
    norms = surface.norms
    thetaRange = 0:2π/coords.M_theta:2π-2π/coords.M_theta
    zetaRange = (0:2π/coords.N_zeta:2π-2π/coords.N_zeta)/coords.N_field_periods
    return MagneticCoordinateGrid(FluxCoordinates,norms.ρ^2*norms.B_ref/2,thetaRange,zetaRange);
end

function (::NE3DLEFromFlux)(x::FluxCoordinates,surface::NE3DLESurface{T}) where {T}
    return x;
end

#NOT POINT-WISE
function (::PestFromNE3DLE)(fl::NE3DLEFieldLine{T}) where {T}
    norms = fl.norms
    #Note alpha_0 is negative and factor of iota different due to GIST convention?
    return MagneticCoordinateCurve(PestCoordinates,norms.ρ^2norms.B_ref/2,fl.params.field_line_label,fl.fl.eta);
end

#NOT POINT-WISE
function (::PestFromNE3DLE)(surface::NE3DLESurface{T}) where {T}
    norms = surface.norms
    return PestCoordinates.(norms.ρ^2*norms.B_ref/2,transpose(surface.coords.theta).-surface.params.iota*surface.coords.zeta,surface.coords.zeta);
end

function (::PestFromFlux)(x::FluxCoordinates, surface::NE3DLESurface{T}) where {T}
    return PestCoordinates(x.ψ,x.θ-surface.params.iota*x.ζ,x.ζ);
end

function (::FluxFromPest)(x::PestCoordinates, surface::NE3DLESurface{T}) where {T}
    return FluxCoordinates(x.ψ,x.α+surface.params.iota*x.ζ,x.ζ);
end

function (::PestFromFlux)(x::FluxCoordinates, fl::NE3DLEFieldLine{T}) where {T}
    return PestCoordinates(x.ψ,x.θ-fl.params.iota*x.ζ,x.ζ);
end

function (::FluxFromPest)(x::PestCoordinates, fl::NE3DLEFieldLine{T}) where {T}
    return FluxCoordinates(x.ψ,x.α+fl.params.iota*x.ζ,x.ζ);
end

#NOT POINT-WISE
function (::CylindricalFromNE3DLE)(surface::NE3DLESurface{T}) where {T}
    return Cylindrical.(transpose(surface.imap.R),transpose(surface.imap.Phi),transpose(surface.imap.Z))
end

#Chunky
function (::CylindricalFromFlux)(x::FluxCoordinates,surface::NE3DLESurface{T}) where {T}
    R = fourier_flux_interpolation(x,surface,surface.imap.R)
    Z = fourier_flux_interpolation(x,surface,surface.imap.Z)
    return Cylindrical(R,-x.ζ,Z)
end

function (::CartesianFromNE3DLE)(surface::NE3DLESurface{T}) where {T}
    c = CylindricalFromNE3DLE()(surface)
    return CartesianFromCylindrical().(c)
end

function (::CartesianFromFlux)(x::FluxCoordinates,surface::NE3DLESurface{T}) where {T}
    c = CylindricalFromFlux()(x,surface)
    return CartesianFromCylindrical()(c)
end

function (::BoozerFromNE3DLE)(surface::NE3DLESurface{T}) where {T}
    coords = surface.coords
    params = surface.params
    geom = surface.geom
    metric = surface.metric
    fft_plan = get_rfft_plan(coords)
    M = coords.M_theta
    N = coords.N_zeta
    M_pol = div(M,2)
    N_tor = div(N,2)
    ι = params.iota
    fft_d1_theta_coeffs = iseven(M) ? vcat(im * rfftfreq(M-1, M-1),[0.0+im*0.0]) : im * rfftfreq(M, M)
    fft_d1_zeta_coeffs = iseven(N) ? coords.N_field_periods * transpose(im * fftfreq(N, N).*(abs.(fftfreq(N, N)).<div(N,2))) : im * coords.N_field_periods * transpose(fftfreq(N,N))
    fft_theta_coeffs = rfftfreq(M, M)
    fft_zeta_coeffs = coords.N_field_periods * transpose(fftfreq(N,N))

    B_cov_theta = @. (metric.g_theta_zeta+ι*metric.g_theta_theta)/metric.sqrtg
    B_cov_zeta = @. (metric.g_zeta_zeta+ι*metric.g_theta_zeta)/metric.sqrtg
    Booz_I = -sum(B_cov_theta)/(M*N)
    Booz_G = -sum(B_cov_zeta)/(M*N)

    Bct_fft_int = (fft_plan*B_cov_theta).*(1 ./fft_d1_theta_coeffs .* (fft_d1_theta_coeffs .!= 0))
    Bcz_fft_int = (fft_plan*B_cov_zeta).*(1 ./fft_d1_zeta_coeffs .* (fft_d1_zeta_coeffs .!= 0))
    if iseven(M)
        Bct_fft_int[M_pol+1,:] .= 0.0+im*0.0
        Bcz_fft_int[M_pol+1,:] .= 0.0+im*0.0
    end
    if iseven(N)
        Bct_fft_int[:,N_tor+1] .= 0.0+im*0.0
        Bcz_fft_int[:,N_tor+1] .= 0.0+im*0.0
    end

    p_fft = Array{ComplexF64}(undef,M_pol+1,N)

    p_fft[1,2:end] = Bcz_fft_int[1,2:end]
    p_fft[2:end,1] = Bct_fft_int[2:end,1]
    p_fft[2:end,2:end] .= (Bcz_fft_int[2:end,2:end].+Bct_fft_int[2:end,2:end])./2
    p_fft[1,1] = 0+im*0
    p_fft ./= Booz_G+ι*Booz_I

    p = fft_plan\p_fft
    return BoozerCoordinates.(norms.ρ^2*norms.B_ref/2,-coords.theta.+surface.params.iota*p,-transpose(coords.zeta).+p)
end

function derivatives(x::FluxCoordinates,surface::NE3DLESurface{T}) where {T}
  	dRdψ = fourier_flux_interpolation(x,surface,surface.imap.R_psi)
  	dZdψ = fourier_flux_interpolation(x,surface,surface.imap.Z_psi)
    dϕdψ = zero(typeof(x.θ))

  	dRdθ = fourier_flux_interpolation(x,surface,surface.imap.R_theta)
  	dZdθ = fourier_flux_interpolation(x,surface,surface.imap.Z_theta)
    dϕdθ = zero(typeof(x.θ))

  	dRdζ = fourier_flux_interpolation(x,surface,surface.imap.R_zeta)
  	dZdζ = fourier_flux_interpolation(x,surface,surface.imap.Z_zeta)
    dϕdζ = -one(typeof(x.θ))
    return @SMatrix [dRdψ dRdθ dRdζ;
                     dϕdψ dϕdθ dϕdζ;
                     dZdψ dZdθ dZdζ]
end

function PlasmaEquilibriumToolkit.basis_vectors(::Covariant,
                       ::CylindricalFromFlux,
                       x::FluxCoordinates,
                       surface::NE3DLESurface{T}
                      ) where {T}
    return derivatives(x,surface)
end

function PlasmaEquilibriumToolkit.basis_vectors(::Covariant,
                       ::CartesianFromFlux,
                       x::FluxCoordinates,
                       surface::NE3DLESurface{T}
                      ) where {T}
    c = CylindricalFromFlux()(x,surface)
    d = derivatives(x,surface)
    sζ, cζ = sincos(x.ζ)
    dXdψ_x = d[1,1]*cζ
    dXdψ_y = -d[1,1]*sζ
    dXdψ_z = d[3,1]

    dXdθ_x = d[1,2]*cζ
    dXdθ_y = -d[1,2]*sζ
    dXdθ_z = d[3,2]

    dXdζ_x = d[1,3]*cζ - c.r*sζ
    dXdζ_y = -d[1,3]*sζ - c.r*cζ
    dXdζ_z = d[3,3]

    return @SMatrix [dXdψ_x dXdθ_x dXdζ_x;
                     dXdψ_y dXdθ_y dXdζ_y;
                     dXdψ_z dXdθ_z dXdζ_z]
end

function PlasmaEquilibriumToolkit.basis_vectors(::Contravariant,
                       ::CartesianFromFlux,
                       x::FluxCoordinates,
                       surface::NE3DLESurface{T}
                      ) where {T}
    covariant_flux_basis = basis_vectors(Covariant(),CartesianFromFlux(),x,surface)
    return PlasmaEquilibriumToolkit.transform_basis(ContravariantFromCovariant(),covariant_flux_basis)
end

function PlasmaEquilibriumToolkit.basis_vectors(::Covariant,
                       ::CartesianFromPest,
                       x::PestCoordinates,
                       surface::NE3DLESurface{T}
                      ) where {T}
    contravariant_pest_basis = basis_vectors(Contravariant(),CartesianFromPest(),x,surface)
    return PlasmaEquilibriumToolkit.transform_basis(CovariantFromContravariant(),contravariant_pest_basis)
end

function PlasmaEquilibriumToolkit.basis_vectors(::Contravariant,
                       ::CartesianFromPest,
                       x::PestCoordinates,
                       surface::NE3DLESurface{T}
                      ) where {T}
    v = FluxFromPest()(x,surface)
    contravariant_flux_basis = basis_vectors(Contravariant(),CartesianFromFlux(),v,surface)
    return PlasmaEquilibriumToolkit.transform_basis(PestFromFlux(),v,contravariant_flux_basis,surface)
end

function PlasmaEquilibriumToolkit.transform_basis(::PestFromFlux,
                         x::FluxCoordinates,
                         e::BasisVectors,
                         surface::NE3DLESurface{T}
                        ) where {T}
    ∇ψ = e[:,1]
    ∇ζ = e[:,3]
    ∇α = -e[:,1]*surface.params.surface_quant["grad_iota"]*x.ζ + e[:,2] - e[:,3]*surface.params.iota
    return hcat(∇ψ,∇α,∇ζ)
end