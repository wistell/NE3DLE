function get_workers()
    return sort(workers())
end

"""
    get_local_array_range(SA::SharedArrays.SharedArray; chunk_dimension=-1)

Returns array of UnitRanges in each dimension of an N dimensional SharedArray and
chunks the array in the direction specified by chunk_dimension. If no chunk_dimension is
specified, then the array is chunked along the last dimension.
"""
function get_local_array_range(SA::SharedArrays.SharedArray; chunk_dimension=-1)
    if length(procs()) > 1
        SA_size = size(SA)
        SA_dims = ndims(SA)
        if chunk_dimension <= 0 || chunk_dimension > SA_dims
            chunk_dimension = SA_dims
        end
        #println("Chunking array along dimension $(chunk_dimension)")
        idx = SharedArrays.indexpids(SA)
        if idx == 0 # This worker is not assigned a piece
            return collect(map(i->UnitRange(1,0),range(1,stop=SA_dims)))
        end
        nchunks = length(procs(SA))
        chunk_range = [round(Int, s) for s in range(0, stop=size(SA,chunk_dimension), length=nchunks+1)]
        ranges = Array{UnitRange{Int}}(undef,SA_dims)
        for i = 1:SA_dims
            ranges[i] = i != chunk_dimension ? UnitRange(1,size(SA,i)) :
                UnitRange(chunk_range[idx]+1,chunk_range[idx+1])
        end
        return ranges
    else
        ranges = collect(map(i->UnitRange(1,size(SA,i)),range(1,stop=ndims(SA))))
        return ranges
    end
end

function get_local_convolution_range(coords::PestGrid,proc_list::Array{Int,1})
    N = coords.N_tor == div(coords.N_zeta,2) ? coords.N_zeta : 2*coords.N_tor + 1
    if length(workers()) > 1
        N_total = 2*N + 1*isodd(N)
        idx = indexin(myid(),proc_list)[1]
        nchunks = length(workers())
        chunk_range = [round(Int, s) for s in range(-N,stop=N-1*isodd(N),length=nchunks+1)]
        return UnitRange(chunk_range[idx]+1,chunk_range[idx+1])
    else
        return UnitRange(-N+1,N-1*isodd(N))
    end
end

function get_local_field_line_range(params::RuntimeParams,proc_list::Array{Int,1})
    nz0 = params.parallel_resolution
    if length(workers()) > 1
        N_total = nz0
        idx = indexin(myid(),proc_list)[1]
        nchunks = length(workers())
        chunk_range = [round(Int, s) for s in range(0,nz0,length=nchunks+1)]
        return UnitRange(chunk_range[idx]+1,chunk_range[idx+1])
    else
        return UnitRange(1,nz0)
    end
end

function get_local_jacobian_range(coords::PestGrid,proc_list::Array{Int,1})
    N = coords.N_zeta
    if length(workers()) > 1
        idx = indexin(myid(),proc_list)[1]
        nchunks = length(workers())
        chunk_range = [round(Int, s) for s in range(0,stop=N,length=nchunks+1)]
        return UnitRange(chunk_range[idx]+1,chunk_range[idx+1])
    else
        return UnitRange(1,N)
    end
end
