function get_fft_plan_1D(coords::PestGrid{T}) where {T}
    fft_plan = 1.0/(coords.M_theta) * plan_fft(Array{T}(undef,coords.M_theta,1))
    inv(fft_plan)
    return fft_plan
end

function get_fft_plan(coords::PestGrid{T}) where {T}
    fft_plan = 1.0/(coords.M_theta * coords.N_zeta) * plan_fft(Array{T}(undef,coords.M_theta,coords.N_zeta))
    inv(fft_plan)
    return fft_plan
end

function get_rfft_plan_1D(coords::PestGrid{T}) where {T}
    fft_plan = 1.0/(coords.M_theta) * plan_rfft(Vector{T}(undef,coords.M_theta))
    inv(fft_plan)
    return fft_plan
end

function get_rfft_plan(coords::PestGrid{T}) where {T}
    fft_plan = 1.0/(coords.M_theta * coords.N_zeta) * plan_rfft(Array{T}(undef,coords.M_theta,coords.N_zeta))
    inv(fft_plan)
    return fft_plan
end

function split_Nyquist_row(fft_data::Array{Complex{T},2}) where {T}
    if iseven(size(fft_data,1))
        temp_data_size = size(fft_data) .+ (1,0)
        nyq_row = div(size(fft_data,1),2)+1
        temp_data = Array{Complex{T}}(undef,temp_data_size)
        for j=1:size(fft_data,2)
            temp_data[1:nyq_row-1,j] = fft_data[1:nyq_row-1,j]
            temp_data[nyq_row+2:size(fft_data,1)+1,j] = fft_data[nyq_row+1:size(fft_data,1),j]
            temp_data[nyq_row:nyq_row+1,j] = 0.5*fft_data[nyq_row,j]*ones(Complex{T},2)
        end
        return temp_data
    else
        return fft_data
    end
end

function combine_Nyquist_row(fft_data::Array{Complex{T},2}) where {T}
    if isodd(size(fft_data,1))
        temp_data_size = size(fft_data) .+ (-1,0)
        nyq_row = div(size(fft_data,1),2)+1
        temp_data = Array{Complex{T}}(undef,temp_data_size)
        for j=1:size(fft_data,2)
            temp_data[1:nyq_row-1,j] = fft_data[1:nyq_row-1,j]
            temp_data[nyq_row+1:size(fft_data,1)-1,j] = fft_data[nyq_row+2:size(fft_data,1),j]
            temp_data[nyq_row,j] = fft_data[nyq_row,j]+fft_data[nyq_row+1,j]
        end
        return temp_data
    else
        return fft_data
    end
end

function split_Nyquist_col(fft_data::Array{Complex{T},2}) where {T}
    if iseven(size(fft_data,2))
        temp_data_size = size(fft_data) .+ (0,1)
        nyq_col = div(size(fft_data,2),2)+1
        temp_data = Array{Complex{T}}(undef,temp_data_size)
        for i=1:size(fft_data,1)
            temp_data[i,1:nyq_col-1] = fft_data[i,1:nyq_col-1]
            temp_data[i,nyq_col+2:size(fft_data,2)+1] = fft_data[i,nyq_col+1:size(fft_data,2)]
            temp_data[i,nyq_col:nyq_col+1] = 0.5*fft_data[i,nyq_col]*ones(Complex{T},2)
        end
        return temp_data
    else
        return fft_data
    end
end

function combine_Nyquist_col(fft_data::Array{Complex{T},2}) where {T}
    if isodd(size(fft_data,2))
        temp_data_size = size(fft_data) .+ (0,-1)
        nyq_col = div(size(fft_data,2),2)+1
        temp_data = Array{Complex{T}}(undef,temp_data_size)
        for i=1:size(fft_data,1)
            temp_data[i,1:nyq_col-1] = fft_data[i,1:nyq_col-1]
            temp_data[i,nyq_col+1:size(fft_data,2)-1] = fft_data[i,nyq_col+2:size(fft_data,2)]
            temp_data[i,nyq_col] = fft_data[i,nyq_col] + fft_data[i,nyq_col+1]
        end
        return temp_data
    else
        return fft_data
    end
end

function split_Nyquist_modes(fft_data::Array{Complex{T},2}) where {T}
    return split_Nyquist_col(split_Nyquist_row(fft_data))
end

function combine_Nyquist_modes(fft_data::Array{Complex{T},2}) where {T}
    return combine_Nyquist_row(combine_Nyquist_col(fft_data))
end

#Converts between indices on a 2D grid to indices in a vector
#that maps the unique coefficients of the grid
function forward_index_map(m::Int,n::Int,M::Int,N::Int)
    if m*n == 0
        if n == 0
            return m+1
        else
            return div(M,2) + 1 + n
        end
    else
        return 1 + div(M,2) + div(N,2) + (n > 0 ? div(M,2)*(n-1) + m : div(M,2)*div(N,2)+(div(M,2)-1)*(div(N,2)-abs(n)-1)+m)
    end
end
#Map indices from a matrix defined specified by positive and negative wavenumber indices
#to indices of a vector containing only the unqiue components of the matrix as
#determined by the reality condition F*₍ᵢⱼ₎ = F₍₋ᵢ₋ⱼ₎.  The dimension over which
#the reality condition is applied is given by the reality_dim parameter, that is for
#the dimension specified by reality_dim, the negative indices for zero and negative indices
#of the non-reality_dim are not computed
function reality_condition_matrix_to_vector_index_map(mat_row_index::Int, mat_col_index::Int, stride_sizes::Tuple{Int,Int}; reality_dim = 2)
    unique_index, reality_index = reality_dim == 2 ? (mat_row_index, mat_col_index) : (mat_col_index, mat_row_index)
    unique_stride_size, reality_stride_size = reality_dim == 2 ? (stride_sizes[1], stride_sizes[2]) : (stride_sizes[2], stride_sizes[1])

    if reality_index >= 0
        if reality_index == 0
            if unique_index >= 0
                return unique_index + 1
            else
                return reality_condition_matrix_to_vector_index_map(-unique_index, reality_index, (unique_stride_size, reality_stride_size); reality_dim = 2)
            end
        else
            return (div(unique_stride_size,2)+1) + unique_stride_size*(reality_index-1) + div(unique_stride_size,2) + unique_index + 1*isodd(unique_stride_size)
        end
    else
        if unique_index != 0
            return reality_condition_matrix_to_vector_index_map(-unique_index, -reality_index, (unique_stride_size, reality_stride_size); reality_dim = 2)
        else
            return reality_condition_matrix_to_vector_index_map(unique_index, -reality_index, (unique_stride_size, reality_stride_size); reality_dim = 2)
        end
    end
end

#Finds unique reality matrix indices from the vector index. Inverse function of reality_condition_matrix_to_vector_index_map
function reality_condition_vector_to_matrix_index_map(vector_index::Int, stride_sizes::Tuple{Int,Int}; reality_dim = 2)
    unique_stride_size, reality_stride_size = reality_dim == 2 ? (stride_sizes[1], stride_sizes[2]) : (stride_sizes[2], stride_sizes[1])
    unique_bound = div(unique_stride_size,2)

    if vector_index <= unique_bound+1
        unique_index = vector_index-1
        reality_index = 0
    else
        index_independent = vector_index-2*div(unique_stride_size,2)-(1+1*isodd(unique_stride_size))
        count_after_zero_reality = vector_index - unique_bound - 2
        unique_index = count_after_zero_reality%unique_stride_size - unique_bound + iseven(unique_stride_size)
        reality_index = div(count_after_zero_reality,unique_stride_size) + 1
    end
    return unique_index,reality_index
end
"""
    fft_backward_reality_condition!(coords::PestGrid{T}, fft_real_coeffs::Array{T,1}, fft_mode_numbers::Array{Int,2}, data::Array{T,2})

Computes the backward FFT taking advantage real coefficients for the FFT.  The transform is computed
at everypoint on the (θ,ζ) grid defined by coords.  The results are inserted into the preallocated
data array.

# Returns
- nothing
"""
function fft_backward_reality_condition!(coords::PestGrid{T},fft_real_coeffs::Array{T,1},
    fft_mode_numbers::Array{Int,2},data::Array{T,2}) where {T}
    #perform some checks on data sizes
    @assert (coords.M_theta,coords.N_zeta) == size(data) "PestGrid{T} $coords and data array $data have incompatible dimensions!"
    if size(fft_real_coeffs,1) != size(fft_mode_numbers,1)
         error("Vector of FFT coefficients ($(:fft_real_coeffs)) with size $(size(fft_real_coeffs,1)) and vector of FFT mode numbers ($(:fft_mode_numbers)) with size $(size(fft_mode_numbers,1)) have incompatible dimensions!")
    end;

    #Filter for only values greater than 1e-8
    filter_inds = findall(abs.(fft_real_coeffs) .>= 1e-8)
    fft_coeffs = fft_real_coeffs[filter_inds]
    fft_inds = fft_mode_numbers[filter_inds,:]

    for n = 1:coords.N_zeta
        for m = 1:coords.M_theta
            temp_val = 0
            for i = 1:size(fft_real_coeffs,1)
                pol_mode = fft_mode_numbers[i,1]
                tor_mode = fft_mode_numbers[i,2]
                if pol_mode == 0 && tor_mode == 0
                    temp_val += fft_real_coeffs[i]
                else
                    temp_val += 2*fft_real_coeffs[i]*cos(pol_mode*coords.theta[m] + coords.N_field_periods*tor_mode*coords.zeta[n])
                end

            end
            data[m,n] = temp_val
        end
    end

    return nothing
end

function fft_backward_reality_condition_parallel!(params::RuntimeParams,coords::PestGrid{T},fft_real_coeffs::Array{T,1},
    fft_mode_numbers::Array{Int,2},data::Array{T,2})  where {T}
    #perform some checks on data sizes
    @assert (coords.M_theta,coords.N_zeta) == size(data) "PestGrid{T} $coords and data array $data have incompatible dimensions!"
    if size(fft_real_coeffs,1) != size(fft_mode_numbers,1)
         error("Vector of FFT coefficients ($(:fft_real_coeffs)) with size $(size(fft_real_coeffs,1)) and vector of FFT mode numbers ($(:fft_mode_numbers)) with size $(size(fft_mode_numbers,1)) have incompatible dimensions!")
    end;

    proc_list = params.proc_list
    resultDict = fill(zeros(T,coords.M_theta,coords.N_zeta),length(proc_list))
    @sync begin
        for i in eachindex(proc_list)
             @async resultDict[i] = remotecall_fetch(fft_backward_reality_condition_kernel!, proc_list[i], coords, fft_real_coeffs, fft_mode_numbers, data, proc_list)
        end
    end

    if length(proc_list) > 1
        for i = 1:length(proc_list)
            data .+= resultDict[i]
        end
    else
        data = resultDict[1]
    end

    return nothing
end

function fft_backward_reality_condition_kernel!(coords::PestGrid{T},fft_real_coeffs::Array{T,1},
    fft_mode_numbers::Array{Int,2},data::Array{T,2},proc_list::Array{Int,1})  where {T}
    #Filter for only values greater than 1e-8
    filter_inds = findall(abs.(fft_real_coeffs) .>= 1e-8)
    fft_coeffs = fft_real_coeffs[filter_inds]
    fft_inds = fft_mode_numbers[filter_inds,:]

    local_index_range = get_local_jacobian_range(coords,proc_list)
    for n = local_index_range
        for m = 1:coords.M_theta
            temp_val = 0
            for i = 1:size(fft_real_coeffs,1)
                pol_mode = fft_mode_numbers[i,1]
                tor_mode = fft_mode_numbers[i,2]
                if pol_mode == 0 && tor_mode == 0
                    temp_val += fft_real_coeffs[i]
                else
                    temp_val += 2*fft_real_coeffs[i]*cos(pol_mode*coords.theta[m] + coords.N_field_periods*tor_mode*coords.zeta[n])
                end

            end
            data[m,n] = temp_val
        end
    end
    return data
end

function extract_reality_indices(coords::PestGrid{T})  where {T}
    M = coords.M_theta
    N = coords.N_zeta

    tor_inds = vcat(zeros(Int,div(M,2)+1),repeat(1:div(N,2),inner=M))
    pol_inds = vcat(collect(0:div(M,2)),repeat(-div(M,2)+1*iseven(M):div(M,2),outer=div(N,2)))

    #Check to make sure lengths are equal
    fft_inds = length(pol_inds) == length(tor_inds) ? hcat(pol_inds,tor_inds) : error("Length of poloidal indices vector $(:pol_inds) with length "*string(length(pol_inds))*" and toroidal indices vector $(:tor_inds) with length "*string(length(tor_inds))*" are not equal!")
    #Check on the number of unique modes in the system
    n_modes = M*div(N,2) + div(M,2) + 1 == size(fft_inds,1) ? M*div(N,2) + div(M,2) + 1 : error("Size of $(fft_inds) is not equal to the number of unique modes!")
    return fft_inds
end

function extract_reality_components(coords::PestGrid{T}, fft_data::Array{T,2}, fft_inds::Array{Int,2})  where {T}
    n_modes = size(fft_inds,1)
    reality_coeffs = Array{T}(undef,n_modes)
    for i=1:n_modes
        reality_coeffs[i] = fft_data[coords.fft_theta_index_map[fft_inds[i,1]], coords.fft_zeta_index_map[fft_inds[i,2]]]
    end

    return reality_coeffs
end

function extract_reality_components(coords::PestGrid{T}, fft_data::Array{T,2})  where {T}
    M = coords.M_theta
    N = coords.N_zeta

    tor_inds = vcat(zeros(Int,div(M,2)+1),repeat(1:div(N,2),inner=M))
    pol_inds = vcat(collect(0:div(M,2)),repeat(-div(M,2)+1*iseven(M):div(M,2),outer=div(N,2)))

    #Check to make sure lengths are equal
    fft_inds = length(pol_inds) == length(tor_inds) ? hcat(pol_inds,tor_inds) : error("Length of poloidal indices vector $(:pol_inds) with length "*string(length(pol_inds))*" and toroidal indices vector $(:tor_inds) with length "*string(length(tor_inds))*" are not equal!")
    #Check on the number of unique modes in the system
    n_modes = M*div(N,2) + div(M,2) + 1 == size(fft_inds,1) ? M*div(N,2) + div(M,2) + 1 : error("Size of $(fft_inds) is not equal to the number of unique modes!")

    reality_coeffs = Array{T}(undef,n_modes)
    for i=1:n_modes
        reality_coeffs[i] = fft_data[coords.fft_theta_index_map[fft_inds[i,1]], coords.fft_zeta_index_map[fft_inds[i,2]]]
    end

    return reality_coeffs, fft_inds

end
