#Chunky
function fourier_flux_interpolation(x::FluxCoordinates,
                                    coords::PestGrid{T},
                                    v::Array{T};
                                    dims::Int=2
                                   ) where {T}
    fft_plan = ndims(v) == 2 ? get_rfft_plan(coords) : println("Not able to interpolate 1D yet")

    M = coords.M_theta
    N = coords.N_zeta
    Nt = coords.N_field_periods
    M_pol = div(M,2)
    N_tor = div(N,2)

    fft_d1_theta_coeffs = im * rfftfreq(M, M)
    fft_d1_zeta_coeffs = transpose(im * Nt * fftfreq(N, N))

    vmn = fft_plan*v
    phase = @. exp(fft_d1_theta_coeffs*x.θ+fft_d1_zeta_coeffs*x.ζ)
    if iseven(M)
        phase[M_pol+1,:] = @. exp(fft_d1_zeta_coeffs*x.ζ)*cos(fft_d1_theta_coeffs[M_pol+1]/im*x.θ)
    end
    if iseven(N)
        phase[:,N_tor+1] = @. exp(fft_d1_theta_coeffs*x.θ)*cos(fft_d1_zeta_coeffs[N_tor+1]/im*x.ζ)
    end
    if iseven(M) && iseven(N)
        phase[M_pol+1,N_tor+1] = cos(fft_d1_theta_coeffs[M_pol+1]*x.θ/im)*cos(fft_d1_zeta_coeffs[N_tor+1]*x.ζ/im)
    end
    vmn_phase = vmn.*phase
    if iseven(M)
        return real(sum(vmn_phase) + sum(conj.(vmn_phase[2:end-1,:])))
    else
        return real(sum(vmn_phase) + sum(conj.(vmn_phase[2:end,:])))
    end
end

function fourier_flux_interpolation(x::FluxCoordinates,
                                    surface::NE3DLESurface{T},
                                    v::Array{T};
                                    dims::Int=2
                                   ) where {T}
    return fourier_flux_interpolation(x,surface.coords,v,dims=dims)
end

function fourier_flux_interpolation(x::AbstractArray,
                                    surface::NE3DLESurface{T},
                                    v::Array{T};
                                    dims::Int=2
                                   ) where {T}
    res = Array{T}(undef, size(x))
    for i in eachindex(x,res)
        res[i] = fourier_flux_interpolation(x[i],surface,v)
    end
    return res
end

function fourier_flux_interpolation(θ::T,
                                    ζ::T,
                                    coords::PestGrid{T},
                                    v::Array{T};
                                    dims::Int=2
                                   ) where {T}
    fft_plan = ndims(v) == 2 ? get_rfft_plan(coords) : println("Not able to interpolate 1D yet")

    M = coords.M_theta
    N = coords.N_zeta
    Nt = coords.N_field_periods
    M_pol = div(M,2)
    N_tor = div(N,2)

    fft_d1_theta_coeffs = im * rfftfreq(M, M)
    fft_d1_zeta_coeffs = transpose(im * Nt * fftfreq(N, N))

    vmn = fft_plan*v
    phase = @. exp(fft_d1_theta_coeffs*θ+fft_d1_zeta_coeffs*ζ)
    if iseven(M)
        phase[M_pol+1,:] = @. exp(fft_d1_zeta_coeffs*ζ)*cos(fft_d1_theta_coeffs[M_pol+1]/im*θ)
    end
    if iseven(N)
        phase[:,N_tor+1] = @. exp(fft_d1_theta_coeffs*θ)*cos(fft_d1_zeta_coeffs[N_tor+1]/im*ζ)
    end
    if iseven(M) && iseven(N)
        phase[M_pol+1,N_tor+1] = cos(fft_d1_theta_coeffs[M_pol+1]*θ/im)*cos(fft_d1_zeta_coeffs[N_tor+1]*ζ/im)
    end
    vmn_phase = vmn.*phase
    if iseven(M)
        return real(sum(vmn_phase) + sum(conj.(vmn_phase[2:end-1,:])))
    else
        return real(sum(vmn_phase) + sum(conj.(vmn_phase[2:end,:])))
    end
end

function fourier_flux_interpolation(θ::T,
                                    ζ::T,
                                    surface::NE3DLESurface{T},
                                    v::Array{T};
                                    dims::Int=2
                                   ) where {T}
    return fourier_flux_interpolation(θ,ζ,surface.coords,v,dims=dims)
end

function fourier_flux_interpolation(η::T,
                                    α0::T,
                                    coords::PestGrid{T},
                                    params::RuntimeParams,
                                    v::Array{T}
                                   ) where {T}
    fft_plan = get_rfft_plan(coords)
    M = coords.M_theta
    N = coords.N_zeta
    Nt = coords.N_field_periods
    M_pol = div(M,2)
    N_tor = div(N,2)
    ι = params.iota

    m = rfftfreq(M, M)
    n = transpose(Nt * fftfreq(N, N))

    vmn = fft_plan*v
    phase = @. exp(im*(m*α0 + (m*ι + n)*η))
    if iseven(M)
        phase[M_pol+1,:] = @. 0.5*(exp(im*(m[M_pol+1]*α0 + (m[M_pol+1]*ι + n)*η)) + exp(im*(-m[M_pol+1]*α0 + (-m[M_pol+1]*ι + n)*η)))
    end
    if iseven(N)
        phase[:,N_tor+1] = @. 0.5*(exp(im*(m*α0 + (m*ι + n[N_tor+1])*η)) + exp(im*(m*α0 + (m*ι - n[N_tor+1])*η)))
    end
    if iseven(M) && iseven(N)
        phase[M_pol+1,N_tor+1] = @. 0.5*(cos(m[M_pol+1]*α0 + (m[M_pol+1]*ι + n[N_tor+1])*η) + cos(m[M_pol+1]*α0 + (m[M_pol+1]*ι - n[N_tor+1])*η))
    end
    vmn_phase = vmn.*phase
    if iseven(M)
        return real(sum(vmn_phase) + sum(conj.(vmn_phase[2:end-1,:])))
    else
        return real(sum(vmn_phase) + sum(conj.(vmn_phase[2:end,:])))
    end
end

function fourier_flux_interpolation(η::Vector{T},
                                    α0::T,
                                    coords::PestGrid{T},
                                    params::RuntimeParams,
                                    v::Array{T}
                                   ) where {T}
    temp_arr = Vector{T}(undef,length(η))
    for i in 1:length(η)
        temp_arr[i] = fourier_flux_interpolation(η[i],α0,coords,params,v)
    end
    return temp_arr
end


function coord_param(coords::PestGrid{T},
                     he_params::HelicalAxisParams{T},
                     R_map::Function,
                     Z_map::Function,
                     Phi_map::Function
                    ) where {T}
    imap = create_inverse_map(coords,R_map,Z_map,Phi_map)

    return imap
end

"""
Because the Miller equilibrium shaping is defined in geometric space, the geometric poloidal angle must be changed to
the straight field line poloidal angle. This requires the derivatives computed in create_inverse_map to be rewritten
using the chain rule. Then R, Z, and their θ derivatives values must be converted from a regular grid in the geometric
poloidal angle to a regular grid in the straight field line poloidal angle. This requres finding the straight field line
poloidal angle value at each regular geometric poloidal angle node, then interpolating R, Z and their θ derivatives.
Need to pass norms to redefine norms.ρ = dψ/dρ / B_ref.
"""
function coord_param(coords::PestGrid{T},
                     m_params::MillerParams{T},
                     R_map::Function,
                     Z_map::Function,
                     Phi_map::Function
                    ) where {T}
    function integrand(θ,x,drR0,s_κ,s_δ,ρ,R0,ζ,s_ζ)
        return (cos(x*sin(θ)-ζ*sin(2*θ)) + drR0*cos(θ+ζ*sin(2*θ))*(1+2*ζ*cos(2*θ)) + ((x*cos(θ)+s_κ*(1+x*cos(θ)))*sin(θ+ζ*sin(2*θ))+(s_ζ*(1+x*sin(θ)*sin(2*θ))-s_δ*sin(θ)*(1+2*ζ*cos(2*θ)))*cos(θ+ζ*sin(2*θ)))*sin(θ+x*sin(θ)))/(R0 + ρ*cos(θ + x*sin(θ)))
        #(cos(x*sin(θ)) + drR0*cos(θ) + (s_κ - s_δ*cos(θ) + (1 + s_κ)*x*cos(θ))*sin(θ)*sin(θ + x*sin(θ)))/(R0 + ρ*cos(θ + x*sin(θ)))
    end

    M = coords.M_theta
    N = coords.N_zeta
    M_pol = div(M,2)
    R0 = m_params.R0
    ρ = m_params.ρ
    κ = m_params.κ
    δ = m_params.δ
    drR0 = m_params.drR0
    s_κ = m_params.s_κ
    s_δ = m_params.s_δ
    ζ = m_params.ζ
    s_ζ = m_params.s_ζ

    fft_plan = get_fft_plan_1D(coords)

    imap = create_inverse_map(coords,R_map,Z_map,Phi_map)

    x = asin(δ)
    γ = quadgk(θ->integrand(θ,x,drR0,s_κ,s_δ,ρ,R0,ζ,s_ζ),0,2*pi)[1]
    
    a0overR = Array{Float64}(undef,M,1)

    for j in 1:M
        θ = coords.theta[j]
        R = imap.R[j,1]
        R_theta = imap.R_theta[j,1]
        R_theta_theta = imap.R_theta_theta[j,1]
        Z_theta = imap.Z_theta[j,1]
        Z_theta_theta = imap.Z_theta_theta[j,1]

		#Set up derivative shifts from geometric to straight field line poloidal angle
        a0 = cos(x*sin(θ)-ζ*sin(2*θ)) + drR0*cos(θ+ζ*sin(2*θ))*(1+2*ζ*cos(2*θ)) + ((x*cos(θ)+s_κ*(1+x*cos(θ)))*sin(θ+ζ*sin(2*θ))+(s_ζ*(1+x*sin(θ)*sin(2*θ))-s_δ*sin(θ)*(1+2*ζ*cos(2*θ)))*cos(θ+ζ*sin(2*θ)))*sin(θ+x*sin(θ))
        a0overR[j] = a0/R

        da0_theta = (2*ζ*cos(2*θ)-x*cos(θ))*sin(x*sin(θ)-ζ*sin(2*θ)) -
                    drR0*(sin(θ+ζ*sin(2*θ))*(1+2*ζ*cos(2*θ))^2 + 4*ζ*sin(2*θ)*cos(θ+ζ*sin(2*θ))) + 
                    s_κ*(cos(θ+ζ*sin(2*θ))*(1+2*ζ*cos(2*θ))*sin(θ+x*sin(θ)) + sin(θ+ζ*sin(2*θ))*(1+x*sin(θ))*cos(θ+x*sin(θ))) +
                    (1+s_κ)*x*(cos(θ)*sin(θ+ζ*sin(2*θ))*(1+x*sin(θ))*cos(θ+x*sin(θ)) + 
                               (cos(θ)*cos(θ+ζ*sin(2*θ))*(1+2*ζ*cos(2*ζ)) - sin(θ)*sin(θ+ζ*sin(2*θ)))*sin(θ+x*sin(θ))) - 
                    s_δ*(((cos(θ)*(1+2*ζ*cos(2*θ)) - 4*ζ*sin(2*θ)*sin(θ))*cos(θ+ζ*sin(2*θ)) - sin(θ)*sin(θ+ζ*sin(2*θ))*(1+2*ζ*cos(2*θ))^2)*sin(θ+x*sin(θ)) + 
                         sin(θ)*(1+2*ζ*cos(2*ζ))*cos(θ+ζ*sin(2*θ))*(1+x*sin(θ))*cos(θ+x*sin(θ))) +
                    s_ζ*(((1+x*cos(θ)*sin(2*θ)+2*x*sin(θ)*cos(2*θ))*cos(θ+ζ*sin(2*θ)) - (1+x*sin(θ)*sin(2*θ))*(1+2*ζ*sin(2*θ))*sin(θ+ζ*sin(2*θ)))*sin(θ+x*sin(θ)) + 
                         (1+x*sin(θ)*sin(2*θ))*cos(θ+ζ*sin(2*θ))*(1+x*sin(θ))*cos(θ+x*sin(θ)))
                    #=
                    -drR0*sin(θ) - x*cos(θ)*sin(x*sin(θ)) +
                    s_κ*(cos(θ)*sin(θ+x*sin(θ)) + sin(θ)*cos(θ+x*sin(θ))*(1+x*cos(θ))) +
                    ((1+s_κ)*x-s_δ)*((cos(θ)^2 - sin(θ)^2)*sin(θ+x*sin(θ)) +
                    cos(θ)*sin(θ)*cos(θ+x*sin(θ))*(1+x*cos(θ)))
                    =#

        dtheta_sfl = γ/(2*pi)*R/a0
        d2theta_sfl = γ/(2*pi)*(dtheta_sfl*(R_theta - R/a0*da0_theta)/a0)

		#Write derivatives of R, Z wrt straight field line poloidal angle
        imap.R_theta[j,1] = R_theta*dtheta_sfl
        imap.R_theta_theta[j,1] = R_theta_theta*(dtheta_sfl)^2 + R_theta*d2theta_sfl
        imap.Z_theta[j,1] = Z_theta*dtheta_sfl
        imap.Z_theta_theta[j,1] = Z_theta_theta*(dtheta_sfl)^2 + Z_theta*d2theta_sfl
    end

    #Find straight field line angle value at each geometric poloidal angle grid node
    sfl_theta = Array{Complex{Float64}}(undef,M,N)
    fft_a0overR = fft_plan * a0overR

    if M_pol == div(M,2)
        fft_theta_inds = Array{Int}(undef,M)
        map!(x-> x <= div(M,2) ? x : x-M, fft_theta_inds, range(0,stop=M-1,step=1))
    else
        fft_theta_inds = Array{Int}(undef,2*M_pol+1)
        map!(x-> x <= M_pol ? x : x-(2*M_pol+1), fft_theta_inds, range(0,stop=2*M_pol,step=1))
    end

    for n = 1:N
        for k = 1:M
            θ = coords.theta[k]
            sfl_theta[k,n] = fft_a0overR[1,1]*θ
            for j in 2:M
                mode = fft_theta_inds[j]
                sfl_theta[k,n] = sfl_theta[k,n] - im/mode*fft_a0overR[j,1]*(exp(im*mode*θ) - 1)
            end
        end
    end

    #To get MxN grid on [0,2π), must append 2π to irregular grid, sfl_theta, and append the R or Z value or derivative at (0,0),
    # because of periodic boundary conditions on θ and ζ.
    sfl_theta = vcat(2*pi/γ*real(sfl_theta),fill(2*pi,(1,N)))

    #Interpolate from irregular grid to regular grid in straight field line poloidal angle
    imap.R[:,1] = linear_interpolation(sfl_theta[:,1], vcat(imap.R[:,1],imap.R[1,1]))(coords.theta)
    imap.R_theta[:,1] = linear_interpolation(sfl_theta[:,1], vcat(imap.R_theta[:,1],imap.R_theta[1,1]))(coords.theta)
    imap.R_theta_theta[:,1] = linear_interpolation(sfl_theta[:,1], vcat(imap.R_theta_theta[:,1],imap.R_theta_theta[1,1]))(coords.theta)
    imap.Z[:,1] = linear_interpolation(sfl_theta[:,1], vcat(imap.Z[:,1],imap.Z[1,1]))(coords.theta)
    imap.Z_theta[:,1] = linear_interpolation(sfl_theta[:,1], vcat(imap.Z_theta[:,1],imap.Z_theta[1,1]))(coords.theta)
    imap.Z_theta_theta[:,1] = linear_interpolation(sfl_theta[:,1], vcat(imap.Z_theta_theta[:,1],imap.Z_theta_theta[1,1]))(coords.theta)

    #Copy first column to every column on grid, because of axisymmetry in the Miller equilibrium.
    for i in 2:N
        imap.R[:,i] .= imap.R[:,1]
        imap.R_theta[:,i] .= imap.R_theta[:,1]
        imap.R_theta_theta[:,i] .= imap.R_theta_theta[:,1]
        imap.Z[:,i] .= imap.Z[:,1]
        imap.Z_theta[:,i] .= imap.Z_theta[:,1]
        imap.Z_theta_theta[:,i] .= imap.Z_theta_theta[:,1]
    end
    
    return imap
end

function coord_param(coords::PestGrid{T},
                     shape::Tuple{Array{T,2},Array{T,2},Array{T,2}},
                     R_map::Array{T,2},
                     Z_map::Array{T,2},
                     Phi_map::Array{T,2}
                    ) where {T}
    imap = create_inverse_map(coords,R_map,Z_map,Phi_map)

    return imap
end
