function shaping(Shape_params::HelicalAxisParams)
    R0 = Shape_params.R0
    ρ = Shape_params.ρ
    Δ = Shape_params.Δ
    Nt = Shape_params.N_turns
    κ = Shape_params.κ
    δ = Shape_params.δ
    x = asin(δ)

    R_map(θ::Float64,ζ::Float64) =  R0 + Δ*cos(Nt*ζ) + ρ*(cos(θ)+(κ-1)*sin(Nt*ζ-θ)*sin(Nt*ζ) - cos(Nt*ζ-θ)*cos(Nt*ζ) + cos(Nt*ζ-θ + x*sin(Nt*ζ-θ))*cos(Nt*ζ))
    Z_map(θ::Float64,ζ::Float64) = Δ*sin(Nt*ζ) + ρ*(sin(θ)-(κ-1)*sin(Nt*ζ-θ)*cos(Nt*ζ) - cos(Nt*ζ-θ)*sin(Nt*ζ) + cos(Nt*ζ-θ + x*sin(Nt*ζ-θ))*sin(Nt*ζ))
    Phi_map(θ::Float64,ζ::Float64) = -ζ

    return R_map, Z_map, Phi_map
end

function shaping(Shape_params::MillerParams)
    R0 = Shape_params.R0
    ρ = Shape_params.ρ
    δ = Shape_params.δ
    κ = Shape_params.κ
    z = Shape_params.ζ

    R_map(θ::Float64,ζ::Float64) = R0 + ρ*cos(θ + asin(δ)*sin(θ))
    Z_map(θ::Float64,ζ::Float64) = κ*ρ*sin(θ+z*sin(2*θ))
    Phi_map(θ::Float64,ζ::Float64) = -ζ

    return R_map, Z_map, Phi_map
end

function shaping(shape::Tuple{Array{T,2}, Array{T,2}, Array{T,2}}) where {T}
    return shape[1], shape[2], shape[3]
end

function get_shape(par_dict::AbstractDict)
    mapping = par_dict["mapping"]
    R0 = par_dict["R0"]
    ρ = par_dict["rho"]
    nfp = par_dict["N_field_periods"]
    
    shape = nothing
    if mapping == "miller"
        nfp == 1  || error("The Miller equilibrium requires N_field_periods = 1.")
        κ = par_dict["kappa"]
        δ = par_dict["delta"]
        drR0 = par_dict["drR0"]
        ζ = par_dict["zeta"]
        if !haskey(par_dict,"s_kappa")
            s_κ = (κ-1)/κ
        else
            s_κ = par_dict["s_kappa"]
        end
        if !haskey(par_dict,"s_delta")
            s_δ = δ/(1-δ^2)^0.5
        else
            s_δ = par_dict["s_delta"]
        end
        if !haskey(par_dict,"s_zeta")
            s_ζ = ζ
        else
            s_ζ = par_dict["s_zeta"]
        end
        shape = MillerParams(R0,ρ,δ,κ,drR0,s_κ,s_δ,ζ,s_ζ)
    elseif mapping == "helical_axis"
        Δ = par_dict["Del"]
        κ = par_dict["kappa"]
        δ = par_dict["delta"]
        shape = HelicalAxisParams(R0,ρ,Δ,nfp,κ,δ)
    elseif mapping == "vmec"
        theta_range = 0:2π/par_dict["M_theta"]:2π-2π/par_dict["M_theta"]
        zeta_range = (0:2π/par_dict["N_zeta"]:2π-2π/par_dict["N_zeta"])/nfp
        flux_coords = MagneticCoordinateGrid(FluxCoordinates, 1.0, theta_range, zeta_range) #Note flux value doesn't really matter
        vmec_surface = VmecSurface(par_dict["s"],readVmecWout(par_dict["vmec_file"]))
        Cyl = CylindricalFromFlux()(flux_coords, vmec_surface);
        R_map = Matrix(transpose(getfield.(Cyl,1)))
        Z_map = Matrix(transpose(getfield.(Cyl,3)))
        Phi_map = Matrix(transpose(getfield.(Cyl,2)))
        shape = (R_map,Z_map,Phi_map)
    end
    return shape
end