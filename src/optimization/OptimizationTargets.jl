#=
function quasisymmetry_deviation(coords::NE3DLE.PestGrid, params::NE3DLE.RuntimeParams, imap::NE3DLE.InverseMap, geom::NE3DLE.MagneticGeometry, metric::NE3DLE.Metric, norms::NE3DLE.NormalizingParams)
    fft_plan = NE3DLE.get_fft_plan(coords)
    M = coords.M_theta
    N = coords.N_zeta
    M_pol = div(M,2)
    N_tor = div(N,2)
    ι = params.iota
    Nt = params.nfp
    coords = NE3DLE.PestGrid(M,N,Nt)

    if iseven(M)
        fft_d1_theta_coeffs = map(i->i < M_pol ? im*i : i > M_pol ? im*(i-M) : 0,
        range(0,stop=M-1))
    else
        fft_d1_theta_coeffs =  map(i->i <= M_pol ? im*i : im*(i-M), range(0,stop=M-1))
    end
    if iseven(N)
        fft_d1_zeta_coeffs = map(i->i < N_tor ? im*coords.N_field_periods*i :
        i > N_tor ? im*coords.N_field_periods*(i-N) : 0, range(0,stop=N-1))
    else
        fft_d1_zeta_coeffs = map(i->i <= N_tor ? im*coords.N_field_periods*i :  im*coords.N_field_periods*(i-N), range(0,stop=N-1))
    end

    B_fft = fft_plan*geom.B_mag

    B_theta_temp = Array{ComplexF64}(undef,M,N)
    B_zeta_temp = Array{ComplexF64}(undef,M,N)
    for j = 1:N
        for i = 1:M
            B_theta_temp[i,j] = fft_d1_theta_coeffs[i]*B_fft[i,j]
            B_zeta_temp[i,j] = fft_d1_zeta_coeffs[j]*B_fft[i,j]
        end
    end
    B_theta = real(fft_plan\B_theta_temp)
    B_zeta = real(fft_plan\B_zeta_temp)

    BdotGradB = (B_zeta.+ ι*B_theta)./metric.sqrtg

    BdGB_fft = fft_plan*BdotGradB
    BdGB_theta_temp = Array{ComplexF64}(undef,M,N)
    BdGB_zeta_temp = Array{ComplexF64}(undef,M,N)
    for j = 1:N
        for i = 1:M
            BdGB_theta_temp[i,j] = fft_d1_theta_coeffs[i]*BdGB_fft[i,j]
            BdGB_zeta_temp[i,j] = fft_d1_zeta_coeffs[j]*BdGB_fft[i,j]
        end
    end
    BdGB_theta = real(fft_plan\BdGB_theta_temp)
    BdGB_zeta = real(fft_plan\BdGB_zeta_temp)
    B2_avg = real((fft_plan*(geom.B_mag.^2 .*metric.sqrtg))[1,1])
    QS_surf = (B_theta.*BdGB_zeta.-B_zeta.*BdGB_theta)./ metric.sqrtg
    QS_triple_vector = sqrt(real((fft_plan*(QS_surf.^2 .*metric.sqrtg)))[1,1]*(norms.B_ref^8/norms.B_0^8/B2_avg^4*norms.R0^4))

    return QS_triple_vector
end
=#

function quasisymmetry_deviation_B(surface::NE3DLESurface)
    coords = surface.coords
    params = surface.params
    geom = surface.geom
    metric = surface.metric
    B_booz = Boozer_spectrum(surface)
    M = coords.M_theta
    N = coords.N_zeta
    M_pol = div(M,2)
    N_tor = div(N,2)
    B00 = B_booz[1,1]
    if params.mapping != "helical_axis"
        for i = 1:M_pol+1
            B_booz[i,i] = 0.0
        end
    else
        B_booz[1,1] = 0.0
        for i = 2:M_pol+1
            B_booz[i,N-i+2] = 0.0
        end
    end
    B_booz_2 = B_booz.^2
    B_booz_2[2:end,1:end] .*= 2
    Booz_numerator = sum(B_booz_2)
    return sqrt(Booz_numerator/B00^2)
end


function quasisymmetry_deviation(surface::NE3DLESurface)
    coords = surface.coords
    params = surface.params
    geom = surface.geom
    metric = surface.metric
    M = coords.M_theta
    N = coords.N_zeta
    if surface.params.mapping == "vmec"
        vmec_s = initialize_from_eq!(par_dict, par_dict["vmec_file"])
        fundamental_mode_index = findall(x->x==maximum(vmec_s.bmn[vmec_s.ntor+2:end].cos),vmec_s.bmn[vmec_s.ntor+2:end].cos)[1]
        QS_helicity = vmec_s.bmn[fundamental_mode_index][2]/vmec_s.bmn[fundamental_mode_index][1]
    else
        QS_helicity = coords.N_field_periods*(1-2*(params.mapping=="helical_axis"))
    end
    ι = params.iota
    B_cov_theta = @. (metric.g_theta_zeta+ι*metric.g_theta_theta)/metric.sqrtg
    B_cov_zeta = @. (metric.g_zeta_zeta+ι*metric.g_theta_zeta)/metric.sqrtg
    I = -sum(B_cov_theta)/(M*N)
    G = -sum(B_cov_zeta)/(M*N)
    C = (G+QS_helicity*I)/(ι-QS_helicity)
    dBdθ = fourier_derivative(coords,geom.B_mag,:dθ)
    dBdζ = fourier_derivative(coords,geom.B_mag,:dζ)
    f_c = @. (B_cov_zeta*dBdθ - B_cov_theta*dBdζ - C*(dBdζ+ι*dBdθ))/metric.sqrtg
    f_c2_avg = flux_surface_avg(surface,f_c.^2)
    B2_avg = flux_surface_avg(surface,geom.B_mag.^2)
    return sqrt(f_c2_avg/B2_avg^3 * (QS_helicity-ι)^2)
end

function toroidal_current(surface::NE3DLESurface{T}) where {T}
    return sum((surface.metric.g_theta_zeta .+ surface.params.iota.*surface.metric.g_theta_theta)./surface.metric.sqrtg)/(surface.params.M_theta.*surface.params.N_zeta)
end

function poloidal_current(surface::NE3DLESurface{T}) where {T}
    return sum((surface.metric.g_zeta_zeta .+ surface.params.iota.*surface.metric.g_theta_zeta)./surface.metric.sqrtg)/(surface.params.M_theta.*surface.params.N_zeta)
end

function rotational_transform(surface::NE3DLESurface{T}) where {T}
    return surface.params.iota
end

function flux_surface_averaged_parallel_current(surface::NE3DLESurface{T}) where {T}
    return surface.params.surface_quant["surface_current"]
end

function global_shear(surface::NE3DLESurface{T}) where {T}
    return -surface.params.surface_quant["grad_iota"]*surface.norms.dpsi_drho*surface.norms.ρ/surface.params.iota
end

function unnormalized_global_shear(surface::NE3DLESurface{T}) where {T}
    return surface.params.surface_quant["grad_iota"]
end

#Normalized to VMEC units
function magnetic_well(surface::NE3DLESurface)
    Vp = surface.norms.Vp
    geom = surface.geom
    metric = surface.metric
    J = metric.sqrtg
    grad_p = surface.params.surface_quant["grad_p"]
    grad_iota = surface.params.surface_quant["grad_iota"]
    ι = surface.params.iota
    μ_0 = surface.norms.mu_0

    B_dot_grad_Lambda = (fourier_derivative(surface.coords,geom.lambda_PS,:dζ) .+ ι.*fourier_derivative(surface.coords,geom.lambda_PS,:dθ))./J
    return (surface.norms.B_ref^2*surface.norms.L_ref^4/4)*Vp*(flux_surface_avg(surface,2*geom.curv_normal./geom.grad_psi_mag) - flux_surface_avg(surface,geom.D_coeff.*B_dot_grad_Lambda/μ_0) + μ_0*grad_p*flux_surface_avg(surface,geom.B_mag.^(-2)) + grad_iota*flux_surface_avg(surface,(metric.g_theta_zeta.+ι*metric.g_theta_theta)./(geom.B_mag.*J).^2))
end

#weighting = 1 means the bounce-point density and curvarture alignment is weighted by minimum(gyy)/gyy
function target_bounce_point_curvature(surface::NE3DLESurface{T};
                                       weighting::Int=0
                                      ) where {T}
    heaviside(t) = 0.5 .* (sign.(t) .+ 1)
    ∇ι =  surface.params.surface_quant["grad_iota"]
    ζ = transpose(surface.coords.zeta)
    D = surface.geom.D_coeff
    ∇ψ = surface.geom.grad_psi_mag
    B = surface.geom.B_mag
    B_ref = surface.norms.B_ref
    dψdρ = surface.norms.dpsi_drho
    Λ = @. (D + ∇ι*ζ)* (∇ψ^2 / B)
    gyy = @. (dψdρ/B_ref * B/∇ψ)^2 * (1 + Λ^2)
    gyy_norm = gyy./minimum(gyy)
    dBdψ = surface.geom.B_mag_psi
    R0 = surface.norms.R0
    physical_quantity = flux_surface_avg(surface,(abs.(B_dot_grad_B(surface)).*dBdψ./B.*(1 .+ (weighting.*(1 .- gyy_norm))./gyy_norm)))
    R03_avg_B2 = R0^3 / sqrt(flux_surface_avg(surface,B.^2))
    x = 1/(physical_quantity*R03_avg_B2)
    return x-heaviside(-x)
end

#=
#Target from E.J. Paul et al., J. Plasma Phys., 87 (2021) with adjustments suggested by T. Krueger
function non_self_intersecting_toroidal_planes(surface::NE3DLESurface{T},
                                               Rmin::T;
                                               exponent::T=2.0
                                              ) where {T}
    δ(x,y) = ==(x,y)
    heaviside(t) = 0.5 * (sign(t) + 1)
    R = surface.imap.R
    Z = surface.imap.Z
    R_θ = surface.imap.R_theta
    Z_θ = surface.imap.Z_theta
    M = surface.coords.M_theta
    N = surface.coords.N_zeta
    delta_xij(i::Int,j::Int,k0::Int) = sqrt((R[i,k0]-R[j,k0])^2 + (Z[i,k0]-Z[j,k0])^2) + δ(i,j)
    tangentj_dot_xi_minus_xj(i::Int,j::Int,k0::Int) = (R_θ[j,k0]*(R[i,k0]-R[j,k0])+Z_θ[j,k0]*(Z[i,k0]-Z[j,k0]))/sqrt(surface.metric.g_theta_theta[j,k0])
    self_contact(i::Int,j::Int,k0::Int) = delta_xij(i,j,k0)*(1-δ(i,j))/(2*sqrt(1-(tangentj_dot_xi_minus_xj(i,j,k0)/delta_xij(i,j,k0))^2))
    objective_surface = zeros(T,M,N)
    for k in 1:N
        for i in 1:M
            for j in 1:M
                x = Rmin - self_contact(i,j,k)
                objective_surface[i,k] += heaviside(x)*(x)^exponent
            end
        end
    end
    return flux_surface_avg(surface,objective_surface)*2*pi/M
end
=#

#Target from T. Krueger 
function non_self_intersecting_toroidal_planes(surface::NE3DLESurface{T},
                                               Rmin::T,
                                               d::T;
                                               exponent::T=2.0,
                                               weight::T=1.0,
                                               new_res::Int=512
                                              ) where {T}
    heaviside(t) = 0.5 .* (sign.(t) .+ 1)
    M = surface.coords.M_theta
    N = surface.coords.N_zeta
    fft_plan = get_rfft_plan(surface.coords)
    fine_fft_plan = 1.0/(new_res * N) * plan_rfft(Array{T}(undef,new_res,N))
    inv(fine_fft_plan)
    R = real.(fine_fft_plan \ vcat(fft_plan*surface.imap.R,zeros(div(new_res,2)-div(M,2),N)))
    Z = real.(fine_fft_plan \ vcat(fft_plan*surface.imap.Z,zeros(div(new_res,2)-div(M,2),N)))
    R_θ = real.(fine_fft_plan \ vcat(fft_plan*surface.imap.R_theta,zeros(div(new_res,2)-div(M,2),N)))
    Z_θ = real.(fine_fft_plan \ vcat(fft_plan*surface.imap.Z_theta,zeros(div(new_res,2)-div(M,2),N)))
    X_θ_mag = sqrt.(R_θ.^2 .+ Z_θ.^2)
    poloidal_curve_length = sum(X_θ_mag,dims=1)*2*π/new_res

    κ0 = 1/Rmin

    D = 2*π/κ0 - 2*asin(d*κ0/2)/κ0

    a = 2*π/κ0 - D

    ΔX(j::Int) = abs.((R.-transpose(R[j,:])).^2 .+ (Z.-transpose(Z[j,:])).^2)
    arclength(i::Int,j::Int) = i<j ? sum(X_θ_mag[i:j,:],dims=1)*(2*π/new_res) : (sum(X_θ_mag[j:end,:],dims=1) .+ sum(X_θ_mag[1:i,:],dims=1))*(2*π/new_res)
    integrand = zeros(new_res,N)

    # heaviside(d .- ΔX(j)) checks if points are too close together
    # heaviside(arclength(i,new_res) .- a) checks if the point is too close to the end of the curve (non-double counting condition)
    # heaviside(arclength(i,j) .- a) checks if points are too close together along the curve (adjacency) (includes both heavisides for t<tbar and t>tbar)

    for i in 1:new_res
        for j in i+1:new_res
            integrand[i,:] .+= (2*π/new_res).*X_θ_mag[i,:].*X_θ_mag[j,:].*transpose(heaviside(arclength(i,new_res).-a)).*heaviside(d.-ΔX(j))[i,:].*transpose(heaviside(arclength(i,j).-a)).*(weight.*(d.-ΔX(j)[i,:])).^exponent
        end
    end

    return (1/N)*sum(sum(integrand,dims=1)./(poloidal_curve_length.^2 .- D.*poloidal_curve_length))*(2*π/new_res)
end


#Target from E.J. Paul et al., J. Plasma Phys., 87 (2021) with adjustments suggested by T. Krueger
function non_intersecting_field_periods(surface::NE3DLESurface{T},
                                        Rmin::T;
                                        exponent::T=2.0,
                                        weight::T=1.0
                                       ) where {T}
    heaviside(t) = 0.5 .* (sign.(t) .+ 1)
    x = Rmin .- surface.imap.R
    return flux_surface_avg(surface,heaviside(x).*(weight*x).^exponent)
end

#Curvature target from T. Krueger et al., J. Plasma Phys., 87 (2021)
function limit_toroidal_plane_curvature(surface::NE3DLESurface{T},
                                        Rmin::T;
                                        exponent::T=2.0,
                                        weight::T=1.0,
                                        new_res::Int=512
                                       ) where {T}
    heaviside(t) = 0.5 .* (sign.(t) .+ 1)
    M = surface.coords.M_theta
    N = surface.coords.N_zeta
    fft_plan = get_rfft_plan(surface.coords)
    fine_fft_plan = 1.0/(new_res * N) * plan_rfft(Array{T}(undef,new_res,N))
    inv(fine_fft_plan)
    R = real.(fine_fft_plan \ vcat(fft_plan*surface.imap.R,zeros(div(new_res,2)-div(M,2),N)))
    R_θ = real.(fine_fft_plan \ vcat(fft_plan*surface.imap.R_theta,zeros(div(new_res,2)-div(M,2),N)))
    Z_θ = real.(fine_fft_plan \ vcat(fft_plan*surface.imap.Z_theta,zeros(div(new_res,2)-div(M,2),N)))
    R_θθ = real.(fine_fft_plan \ vcat(fft_plan*surface.imap.R_theta_theta,zeros(div(new_res,2)-div(M,2),N)))
    Z_θθ = real.(fine_fft_plan \ vcat(fft_plan*surface.imap.Z_theta_theta,zeros(div(new_res,2)-div(M,2),N)))
        
    poloidal_curve_length = sum(sqrt.(R_θ.^2 .+ Z_θ.^2),dims=1)*2*π/new_res

    κ = @. abs(R*(Z_θ*R_θθ-R_θ*Z_θθ))/(R_θ^2+Z_θ^2)^(3/2)
    κ0 = 1/Rmin

    x = κ .- κ0

    integrand = @. sqrt(R_θ^2 + Z_θ^2)*heaviside(x)*(weight*x)^exponent
    return (1/N)*sum(integrand./poloidal_curve_length)
end

function average_triangularity(surface::NE3DLESurface{T}) where {T}
    R = surface.imap.R
    Z = surface.imap.Z
    M = surface.coords.M_theta
    N = surface.coords.N_zeta
    Nt = surface.coords.N_field_periods
    ζ = transpose(surface.coords.zeta).*ones(M,N)
    fft_plan = NE3DLE.get_rfft_plan(surface.coords)
    R0 = surface.norms.R0
    Δ = 2*real(fft_plan*R)[1,end]
    R_prime = @. (R-R0)*cos(Nt*ζ) + Z*sin(Nt*ζ) - Δ + R0
    Z_prime = @. Z*cos(Nt*ζ) - (R-R0)*sin(Nt*ζ)
    Rmax = maximum(R_prime,dims=1)
    Rmin = minimum(R_prime,dims=1)
    Rzmax = Array{T}(undef,1,N)
    Rzmin = Array{T}(undef,1,N)
    for k in 1:N
        Rzmax[1,k] = R_prime[findall(x->x==maximum(Z_prime[:,k]),Z_prime[:,k])[1],k]
        Rzmin[1,k] = R_prime[findall(x->x==minimum(Z_prime[:,k]),Z_prime[:,k])[1],k]
    end
    Rgeo = (Rmax .+ Rmin)./2
    a = (Rmax .- Rmin)./2
    delta_l = (Rgeo.-Rzmin)./a
    delta_u = (Rgeo.-Rzmax)./a
    return sum(delta_l.+delta_u)/(2*N)
end

function average_elongation(surface::NE3DLESurface{T}) where {T}
    R = surface.imap.R
    Z = surface.imap.Z
    M = surface.coords.M_theta
    N = surface.coords.N_zeta
    Nt = surface.coords.N_field_periods
    ζ = transpose(surface.coords.zeta).*ones(M,N)
    fft_plan = NE3DLE.get_rfft_plan(surface.coords)
    R0 = surface.norms.R0
    Δ = 2*real(fft_plan*R)[1,end]
    R_prime = @. (R-R0)*cos(Nt*ζ) + Z*sin(Nt*ζ) - Δ + R0
    Z_prime = @. Z*cos(Nt*ζ) - (R-R0)*sin(Nt*ζ)
    Rmax = maximum(R_prime,dims=1)
    Rmin = minimum(R_prime,dims=1)
    Zmax = Array{T}(undef,1,N)
    Zmin = Array{T}(undef,1,N)
    Zrmax = Array{T}(undef,1,N)
    for k in 1:N
        Zrmax[1,k] = Z_prime[findall(x->x==maximum(R_prime[:,k]),R_prime[:,k])[1],k]
        Zmax[1,k] = maximum(Z_prime[:,k])
        Zmin[1,k] = minimum(Z_prime[:,k])
    end
    a = (Rmax .- Rmin)./2
    kappa_u = (Zmax .- Zrmax)./a
    kappa_l = (Zrmax .- Zmin)./a
    return sum((kappa_u .+ kappa_l))/(2*N)
end

function average_aspect_ratio(surface::NE3DLESurface{T}) where {T}
    R = surface.imap.R
    Z = surface.imap.Z
    M = surface.coords.M_theta
    N = surface.coords.N_zeta
    Nt = surface.coords.N_field_periods
    ζ = transpose(surface.coords.zeta).*ones(M,N)
    fft_plan = NE3DLE.get_rfft_plan(surface.coords)
    R0 = surface.norms.R0
    Δ = 2*real(fft_plan*R)[1,end]
    R_prime = @. (R-R0)*cos(Nt*ζ) + Z*sin(Nt*ζ) - Δ + R0
    Z_prime = @. Z*cos(Nt*ζ) - (R-R0)*sin(Nt*ζ)
    Rmax = maximum(R_prime,dims=1)
    Rmin = minimum(R_prime,dims=1)
    
    a = (Rmax .- Rmin)./2
    #Rgeo = (Rmax .+ Rmin)./2
    return sum(abs.(surface.norms.R0./a))/(N)
end
#=
function limit_gyy(surface::NE3DLESurface{T},target::T) where {T}
    field_line = NE3DLEFieldLine(surface)
    limit_gyy(field_line,target)
end

function limit_gyy(field_line::NE3DLEFieldLine{T},target::T) where {T}
    gene = field_line.gene
    limit_gyy(gene,target)
end

function limit_gyy(gene::GeneGeom{T},target::T) where {T}
    limit_gyy(gene.gyy,gene.gxx,target,gene.n_pol,gene.gridpoints)
end

function limit_gyy(gyy::Vector{T},gxx::Vector{T},target::T,n_pol::Int,nz0::Int) where {T}
    if nz0%(n_pol) != 0
        @warn "limit_gyy: Number of gridpoints not evenly divisible by the number of poloidal turns. No guarantee of accurate [-pi,pi) evalutation."
    end
    heaviside(t) = 0.5 .* (sign.(t) .+ 1)
    nz_per_pi = div(nz0,2*n_pol)
    minus_pi_index = (n_pol-1)*nz_per_pi
    pi_index = (n_pol+1)*nz_per_pi
    return (maximum(gyy[minus_pi_index:pi_index]./gxx[minus_pi_index:pi_index])-target)*heaviside(maximum(gyy[minus_pi_index:pi_index]./gxx[minus_pi_index:pi_index])-target)
end
=#