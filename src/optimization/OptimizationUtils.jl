"""
Boozer_spectrum(surface::NE3DLESurface)

Computes Boozer spectrum of magnetic field strength. Input is a NE3DLESurface, output is fourier modes of |B| in Boozer coordinates.
"""
function Boozer_spectrum(surface::NE3DLESurface)
    coords = surface.coords
    params = surface.params
    geom = surface.geom
    metric = surface.metric
    fft_plan = get_rfft_plan(coords)
    M = coords.M_theta
    N = coords.N_zeta
    M_pol = div(M,2)
    N_tor = div(N,2)
    ι = params.iota
    fft_d1_theta_coeffs = iseven(M) ? vcat(im * rfftfreq(M-1, M-1),[0.0+im*0.0]) : im * rfftfreq(M, M)
    fft_d1_zeta_coeffs = iseven(N) ? coords.N_field_periods * transpose(im * fftfreq(N, N).*(abs.(fftfreq(N, N)).<div(N,2))) : im * coords.N_field_periods * transpose(fftfreq(N,N))
    fft_theta_coeffs = rfftfreq(M, M)
    fft_zeta_coeffs = coords.N_field_periods * transpose(fftfreq(N,N))

    B_cov_theta = @. (metric.g_theta_zeta+ι*metric.g_theta_theta)/metric.sqrtg
    B_cov_zeta = @. (metric.g_zeta_zeta+ι*metric.g_theta_zeta)/metric.sqrtg
    Booz_I = -sum(B_cov_theta)/(M*N)
    Booz_G = -sum(B_cov_zeta)/(M*N)

    Bct_fft_int = (fft_plan*B_cov_theta).*(1 ./fft_d1_theta_coeffs .* (fft_d1_theta_coeffs .!= 0))
    Bcz_fft_int = (fft_plan*B_cov_zeta).*(1 ./fft_d1_zeta_coeffs .* (fft_d1_zeta_coeffs .!= 0))
    if iseven(M)
        Bct_fft_int[M_pol+1,:] .= 0.0+im*0.0
        Bcz_fft_int[M_pol+1,:] .= 0.0+im*0.0
    end
    if iseven(N)
        Bct_fft_int[:,N_tor+1] .= 0.0+im*0.0
        Bcz_fft_int[:,N_tor+1] .= 0.0+im*0.0
    end

    p_fft = Array{ComplexF64}(undef,M_pol+1,N)

    p_fft[1,2:end] = Bcz_fft_int[1,2:end]
    p_fft[2:end,1] = Bct_fft_int[2:end,1]
    p_fft[2:end,2:end] .= (Bcz_fft_int[2:end,2:end].+Bct_fft_int[2:end,2:end])./2
    p_fft[1,1] = 0+im*0
    p_fft ./= Booz_G+ι*Booz_I

    p = fft_plan\p_fft

    p_theta = fourier_derivative(coords,p,:dθ)
    p_zeta = fourier_derivative(coords,p,:dζ)

    B_booz = zeros(ComplexF64,M_pol+1,N)
    B2_transform = geom.B_mag .* (1 .- p_zeta .- ι*p_theta)
    for k in 1:N
        for l in 1:M
            ζ = coords.zeta[k]
            θ = coords.theta[l]
            P = p[l,k]
            phase = @. exp(-im*(fft_theta_coeffs*(-θ+ι*P)+fft_zeta_coeffs*(-ζ+P)))/(M*N)
            if iseven(M)
                phase[M_pol+1,:] = @. exp(-im*fft_zeta_coeffs*(-ζ+P))*cos(fft_theta_coeffs[M_pol+1]*(-θ+ι*P))/(M*N)
            end
            if iseven(N)
                phase[:,N_tor+1] = @. exp(-im*fft_theta_coeffs*(-θ+ι*P))*cos(-fft_zeta_coeffs[N_tor+1]*(-ζ+P))/(M*N)
            end
            if iseven(M) && iseven(N)
                phase[M_pol+1,N_tor+1] = cos(fft_theta_coeffs[M_pol+1]*(-θ+ι*P))*cos(fft_zeta_coeffs[N_tor+1]*(-ζ+P))/(M*N)
            end
            B_booz .+= B2_transform[l,k].*phase
        end
    end
    return real.(B_booz)
end

"""
inverse_Boozer_spectrum(B_booz_spec::Array{T,2},surface::NE3DLESurface)

Computes surface quantities in real space on evenly spaced magnetic flux coordinate grid. 
all(inverse_Boozer_spectrum(Boozer_spectrum(surface),surface) .≈ surface.geom.B_mag) should be true
Input is an array and a NE3DLESurface, output is the array converted to real space on evenly spaced magnetic flux coordinate grid.
"""

function inverse_Boozer_spectrum(B_booz_spec::Array{T,2},surface::NE3DLESurface{T}) where {T}
    coords = surface.coords
    params = surface.params
    geom = surface.geom
    metric = surface.metric
    fft_plan = get_rfft_plan(coords)
    M = coords.M_theta
    N = coords.N_zeta
    M_pol = div(M,2)
    N_tor = div(N,2)
    ι = params.iota
    fft_d1_theta_coeffs = iseven(M) ? vcat(im * rfftfreq(M-1, M-1),[0.0+im*0.0]) : im * rfftfreq(M, M)
    fft_d1_zeta_coeffs = iseven(N) ? coords.N_field_periods * transpose(im * fftfreq(N, N).*(abs.(fftfreq(N, N)).<div(N,2))) : im * coords.N_field_periods * transpose(fftfreq(N,N))
    fft_theta_coeffs = rfftfreq(M, M)
    fft_zeta_coeffs = coords.N_field_periods * transpose(fftfreq(N,N))

    B_cov_theta = @. (metric.g_theta_zeta+ι*metric.g_theta_theta)/metric.sqrtg
    B_cov_zeta = @. (metric.g_zeta_zeta+ι*metric.g_theta_zeta)/metric.sqrtg
    Booz_I = -sum(B_cov_theta)/(M*N)
    Booz_G = -sum(B_cov_zeta)/(M*N)

    Bct_fft_int = (fft_plan*B_cov_theta).*(1 ./fft_d1_theta_coeffs .* (fft_d1_theta_coeffs .!= 0))
    Bcz_fft_int = (fft_plan*B_cov_zeta).*(1 ./fft_d1_zeta_coeffs .* (fft_d1_zeta_coeffs .!= 0))
    if iseven(M)
        Bct_fft_int[M_pol+1,:] .= 0.0+im*0.0
        Bcz_fft_int[M_pol+1,:] .= 0.0+im*0.0
    end
    if iseven(N)
        Bct_fft_int[:,N_tor+1] .= 0.0+im*0.0
        Bcz_fft_int[:,N_tor+1] .= 0.0+im*0.0
    end

    p_fft = Array{ComplexF64}(undef,M_pol+1,N)

    p_fft[1,2:end] = Bcz_fft_int[1,2:end]
    p_fft[2:end,1] = Bct_fft_int[2:end,1]
    p_fft[2:end,2:end] .= (Bcz_fft_int[2:end,2:end].+Bct_fft_int[2:end,2:end])./2
    p_fft[1,1] = 0+im*0
    p_fft ./= Booz_G+ι*Booz_I

    p = fft_plan\p_fft

    p_theta = fourier_derivative(coords,p,:dθ)
    p_zeta = fourier_derivative(coords,p,:dζ)

    # p, dp/dθ, dp/dζ on even grid spacing of flux coordinates now must be transformed to even grid spacing in Boozer coordinates

    p_transform = p .* (1 .- p_zeta .- ι*p_theta)
    p_theta_transform = p_theta .* (1 .- p_zeta .- ι*p_theta)
    p_zeta_transform = p_zeta .* (1 .- p_zeta .- ι*p_theta)
    p_booz_mn = zeros(ComplexF64,M_pol+1,N)
    p_theta_booz_mn = zeros(ComplexF64,M_pol+1,N)
    p_zeta_booz_mn = zeros(ComplexF64,M_pol+1,N)

    for k in 1:N
        for l in 1:M
            ζ = coords.zeta[k]
            θ = coords.theta[l]
            P = p[l,k]
            phase = @. exp(-im*(fft_theta_coeffs*(-θ+ι*P)+fft_zeta_coeffs*(-ζ+P)))/(M*N)
            if iseven(M)
                phase[M_pol+1,:] = @. exp(-im*fft_zeta_coeffs*(-ζ+P))*cos(fft_theta_coeffs[M_pol+1]*(-θ+ι*P))/(M*N)
            end
            if iseven(N)
                phase[:,N_tor+1] = @. exp(-im*fft_theta_coeffs*(-θ+ι*P))*cos(-fft_zeta_coeffs[N_tor+1]*(-ζ+P))/(M*N)
            end
            if iseven(M) && iseven(N)
                phase[M_pol+1,N_tor+1] = cos(fft_theta_coeffs[M_pol+1]*(-θ+ι*P))*cos(fft_zeta_coeffs[N_tor+1]*(-ζ+P))/(M*N)
            end
            p_booz_mn .+= p_transform[l,k].*phase
            p_theta_booz_mn .+= p_theta_transform[l,k].*phase
            p_zeta_booz_mn .+= p_zeta_transform[l,k].*phase
        end
    end

    p_booz = fft_plan \ p_booz_mn
    p_theta_booz = fft_plan \ p_theta_booz_mn
    p_zeta_booz = fft_plan \ p_zeta_booz_mn
    # With p, dp/dθ, dp/dζ evenly spaced in Boozer coordinates, transforming B_booz to evenly spaced pest coordinates begins
    # Note, θ and ζ are the Boozer poloidal and toroidal angles in the phase variable
    B = zeros(ComplexF64,M_pol+1,N)
    B_booz = fft_plan \ B_booz_spec
    B_inv_transform = B_booz ./ (1 .- p_zeta_booz .- ι*p_theta_booz)
    for k in 1:N
        for l in 1:M
            ζ = coords.zeta[k]
            θ = coords.theta[l]
            P = p_booz[l,k]
            phase = @. exp(-im*(fft_theta_coeffs*(-θ+ι*P)+fft_zeta_coeffs*(-ζ+P)))/(M*N)
            if iseven(M)
                phase[M_pol+1,:] = @. exp(-im*fft_zeta_coeffs*(-ζ+P))*cos(fft_theta_coeffs[M_pol+1]*(-θ+ι*P))/(M*N)
            end
            if iseven(N)
                phase[:,N_tor+1] = @. exp(-im*fft_theta_coeffs*(-θ+ι*P))*cos(-fft_zeta_coeffs[N_tor+1]*(-ζ+P))/(M*N)
            end
            if iseven(M) && iseven(N)
                phase[M_pol+1,N_tor+1] = cos(fft_theta_coeffs[M_pol+1]*(-θ+ι*P))*cos(fft_zeta_coeffs[N_tor+1]*(-ζ+P))/(M*N)
            end
            B .+= B_inv_transform[l,k].*phase
        end
    end
    return fft_plan \ B
end
