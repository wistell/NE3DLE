using .StellaratorOptimization

function StellOpt.EquilibriumWrapper(par_dict::AbstractDict, ::Type{E}) where {E <: NE3DLESurface}
    return StellOpt.EquilibriumWrapper{Float64, E, StellOpt.SPSA}(par_dict)
end

function StellOpt.EquilibriumWrapper(par_dict::AbstractDict, ::Type{E}, grad_method::D = StellOpt.SPSA()) where {E <: NE3DLESurface, D <: StellOpt.AbstractDerivative}
    T = data_type(eq)
    return StellOpt.EquilibriumWrapper{T, E, D}(par_dict, eq, grad_method)
end

function StellOpt.EquilibriumWrapper(par_dict::AbstractDict, eq::E, grad_method::D = StellOpt.SPSA()) where {E <: NE3DLESurface, D <: StellOpt.AbstractDerivative}
    T = data_type(eq)
    return StellOpt.EquilibriumWrapper{T, E, D}(par_dict, eq, grad_method)
end

function StellOpt.EquilibriumWrapper{T, E, D}(par_dict::AbstractDict, eq::E, grad_method::D = StellOpt.SPSA()) where {T, E <: NE3DLESurface, D <: StellOpt.AbstractDerivative}
    translator = Dict{Int, Symbol}()
    geometries = Dict{UInt, Expr}()
    return StellOpt.EquilibriumWrapper{T, E, D}(eq, par_dict, translator, geometries, grad_method)
end

function StellOpt.EquilibriumWrapper{T, E, D}(par_dict::AbstractDict) where {T, E <: NE3DLESurface, D <: StellOpt.AbstractDerivative}
    input = empty(par_dict)
    if keytype(par_dict) === Symbol
        for (key, val) in par_dict
            input[string(key)] = val
        end
    elseif keytype(par_dict) === String
        input = par_dict
    else
        error("Unknown keytype for input parameter dictionary")
    end
    eq = call_NE3DLE(input)
    return StellOpt.EquilibriumWrapper{T, E, D}(input, eq)
end

function StellOpt.EquilibriumWrapper(input_file::AbstractString, ::Type{E}) where {E <: NE3DLESurface}
    return StellOpt.EquilibriumWrapper{Float64, E, StellOpt.SPSA}(input_file)
end

function StellOpt.EquilibriumWrapper{T, E, D}(input_file::AbstractString) where {T, E <: NE3DLESurface, D <: StellOpt.AbstractDerivative}
    pars = init_parDict(input_file)
    return StellOpt.EquilibriumWrapper{T, E, D}(pars)
end

#Returns the surface
function StellOpt.compute_equilibrium(wrapper::StellOpt.EquilibriumWrapper{T, E, D}) where {T, E <: NE3DLESurface, D <: StellOpt.AbstractDerivative}
    input = OrderedDict{String, Any}()
    if keytype(wrapper.input) === Symbol
        for (key, value) in wrapper.input
            input[string(key)] = value
        end
    else
        input = wrapper.input
    end
    return call_NE3DLE(input)
end

"""
        function translate!(wrapper::EquilibriumWrapper{T, E, D})

The NE3DLE parDict is Dict{String,Any}, and the translator is Dict{Int,Symbol}.
For most inputs, the translation is just a key converted from Symbol to String.
For optimization variables Rmn(i,j) or Zmn(i,j), where i,j are mode numbers,
the symbol is converted into a string, the modes split from the mapping
variable, then stored in parDict (a.k.a. ne3dle_input) as a
Dict{Tuple{Int,Int},Float64} (ComplexF64?).
"""
function StellOpt.translate!(wrapper::StellOpt.EquilibriumWrapper{T, E, D},
                             state_vector::Vector{V};
                            ) where {T, E <: NE3DLESurface, D <: StellOpt.AbstractDerivative, V <: StellOpt.AbstractOptVariable}
    #length(state_vector) == length(opt.translator) || error("State vector and translation dictionary must have the same length")
    
    function parsetuple(s::AbstractString)
        return Tuple(parse.(Int, split(s, ",")))
    end

    for v in state_vector
        str_key = string(v.name)
        if occursin("Rmn", str_key) || occursin("Zmn", str_key)
            var, modes = split(str_key, "(")
            modes = split(String(modes), ")")[1]
            modes = parsetuple(modes)
            if haskey(wrapper.input, var)
                wrapper.input[var][modes] = v.value
            else
                wrapper.input[var] = OrderedDict{Tuple{Int, Int}, T}()
                wrapper.input[var][modes] = v.value
            end
        else
            if haskey(wrapper.input,str_key)
                wrapper.input[str_key] = v.value
            else
                error("Unknown optimization parameter")
            end
        end
    end
end

function inverse_translate!(wrapper::StellOpt.EquilibriumWrapper{T, E, D},
                            state_vector::Vector{StellOpt.OptimizationVariable};
                           ) where {T, E <: NE3DLESurface, D <: StellOpt.AbstractDerivative, V <: StellOpt.AbstractOptVariable}

    function parsetuple(s::AbstractString)
        return Tuple(parse.(Int, split(s, ",")))
    end

    for index in keys(state_vector)
        str_key = string(wrapper.translator[index])
        if occursin("Rmn", str_key) || occursin("Zmn", str_key)
            var, modes = split(str_key, "(")
            modes = split(String(modes), ")")[1]
            modes = parsetuple(modes)
            if haskey(wrapper.input[var],modes)
                state_vector[index].value  = wrapper.input[var][modes]  
            end    
        end
    end
end

function StellOpt.add_variable!(prob::StellOpt.Problem{T}, 
                                wrapper::StellOpt.EquilibriumWrapper{T, E, D},
                                name::Union{AbstractString, Symbol};
                                kwargs...
                               ) where {T, E <: NE3DLESurface, D <: StellOpt.AbstractDerivative}
    function parsetuple(s::AbstractString)
        return Tuple(parse.(Int, split(s, ",")))
    end
    if typeof(name) <: Symbol
        name_string = replace(string(name), " " => "")
    else
        name_string = name
    end
    symbol_name = Symbol(name_string)
    var_value = zero(T)
    if occursin("Rmn", name_string) || occursin("Zmn", name_string)
        var, modes = split(name_string, "(")
        modes = split(String(modes), ")")[1]
        modes = parsetuple(modes)
        if haskey(wrapper.input, var)
            if haskey(wrapper.input[var], modes)
                var_value = wrapper.input[var][modes]
            else
                @warn "Variable $(name_string) has no specified initial value, setting to 0"
            end
        else
            @warn "Variable $(name_string) has no specified initial value, setting to 0"
        end
    elseif haskey(wrapper.input, name_string)
        var_value = wrapper.input[name_string]
    else
        @warn "Variable $(name_string) has no specified initial value, setting to 0"
    end
    @debug "Wrapper objectid: $(objectid(wrapper))"
    index = StellOpt.add_variable!(prob, StellOpt.OptimizationVariable(symbol_name, var_value, objectid(wrapper); 
                                     kwargs...))
    StellOpt.update_translator!(wrapper, index, symbol_name)
    return nothing
end

function NE3DLESurface(wrapper::StellOpt.EquilibriumWrapper{T, E, D}) where {T, E <: NE3DLESurface, D <: StellOpt.AbstractDerivative}
    return wrapper.eq
end

function StellOpt.extract_derived_geometry(wrapper::StellOpt.EquilibriumWrapper{T, E, D},
                                            key::UInt;
                                          ) where {T, E <: NE3DLESurface, D <: StellOpt.AbstractDerivative}
    #geom_call = copy(wrapper.derived_geometries[key])
    #geom_slot = findlast(x -> x === :equilibrium, geom_call.args)
    #geom_call.args[geom_slot] = wrapper.eq
    geom_call = Expr(wrapper.derived_geometries[key].head, 
                     replace(wrapper.derived_geometries[key].args,
                             :equilibrium => wrapper.eq)...)
    return eval(geom_call)
end

function StellOpt.write_input_file(wrapper::StellOpt.EquilibriumWrapper{T, E, D},
                                   filename::String;
                                  ) where {T, E <: NE3DLESurface, D <: StellOpt.AbstractDerivative}
    write_input_to_hdf5_file(wrapper.input, fname = filename)
    return nothing
end
