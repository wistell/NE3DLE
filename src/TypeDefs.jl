abstract type AbstractShapingParameters end

#=
# File containing defintions for the structs used in the local 3D MHD equilibrium calcluation
=#
using Primes: factor
#Define a struct with the grid in straight-field-line coordinates with the accompanying Fourier
#modes and coefficients for derivatives

#The wavenumber range is defined by (-M_theta/2,M_theta/2) and
#(-N_zeta/2,N_zeta/2), but the data is represented in the traditional
#way for ffts, where indices (1,N/2-1) are the zero and positive wavenumbers
#and indices (N/2+1,N) are the negative wavenumber in ascending order
#If either M_theta or N_zeta are even, the mode at index N is the
#Nyquist wavenumber

#NOTE: the Julia implementation of the forward FFT is defined by a
#positive sign convention for im, i.e. f(x) = Σₖ Fₖ exp(ıkx), thus the
#derivative operators have a positive sign

struct PestGrid{T}
    M_theta::Int
    N_zeta::Int
    N_field_periods::Int
    theta::Vector{T}
    zeta::Vector{T}
end

function PestGrid(m::Int,n::Int,nfp::Int; force_size=false, float_type::Type = Float64)
    if force_size
        if isodd(m)
            M_theta = findmin(maximum(first.(factor(m - 1).pe)), maximum(first.(factor(m + 1).pe))) == 2 ? m + 1 : m - 1
        else
            M_theta = m
        end
        if isodd(n)
            N_zeta = findmin(maximum(first.(factor(n - 1).pe)), maximum(first.(factor(n + 1).pe))) == 2 ? n + 1 : n - 1
        else
            N_zeta = n
        end
    else
        M_theta = m
        N_zeta = n
    end
    delta_theta = 2π/M_theta
    delta_zeta = 2π/(N_zeta*nfp)
    theta = delta_theta * range(0, stop = M_theta - 1,step = 1)
    zeta = delta_zeta * range(0, stop = N_zeta - 1, step = 1)
    return PestGrid{float_type}(M_theta, N_zeta, nfp, theta, zeta)
end

struct InverseMap{T}
    R::Array{T,2}
    R_psi::Array{T,2}
    R_theta::Array{T,2}
    R_zeta::Array{T,2}
    R_psi_theta::Array{T,2}
    R_psi_zeta::Array{T,2}
    R_theta_theta::Array{T,2}
    R_theta_zeta::Array{T,2}
    R_zeta_zeta::Array{T,2}
    Z::Array{T,2}
    Z_psi::Array{T,2}
    Z_theta::Array{T,2}
    Z_zeta::Array{T,2}
    Z_psi_theta::Array{T,2}
    Z_psi_zeta::Array{T,2}
    Z_theta_theta::Array{T,2}
    Z_theta_zeta::Array{T,2}
    Z_zeta_zeta::Array{T,2}
    Phi::Array{T,2}
end

function InverseMap(M::Int,N::Int; float_type::Type = Float64)
    R = zeros(float_type,M,N)
    R_psi = zeros(float_type,M,N)
    R_theta = zeros(float_type,M,N)
    R_zeta = zeros(float_type,M,N)
    R_psi_theta = zeros(float_type,M,N)
    R_psi_zeta = zeros(float_type,M,N)
    R_theta_theta = zeros(float_type,M,N)
    R_theta_zeta = zeros(float_type,M,N)
    R_zeta_zeta = zeros(float_type,M,N)
    Z = zeros(float_type,M,N)
    Z_psi = zeros(float_type,M,N)
    Z_theta = zeros(float_type,M,N)
    Z_zeta = zeros(float_type,M,N)
    Z_psi_theta = zeros(float_type,M,N)
    Z_psi_zeta = zeros(float_type,M,N)
    Z_theta_theta = zeros(float_type,M,N)
    Z_theta_zeta = zeros(float_type,M,N)
    Z_zeta_zeta = zeros(float_type,M,N)
    Phi = zeros(float_type,M,N)
    return InverseMap{float_type}(R,R_psi,R_theta,R_zeta,R_psi_theta,R_psi_zeta,R_theta_theta,R_theta_zeta,R_zeta_zeta,Z,Z_psi,Z_theta,Z_zeta,Z_psi_theta,Z_psi_zeta,Z_theta_theta,Z_theta_zeta,Z_zeta_zeta,Phi)
end

struct Metric{T}
    sqrtg::Array{T}
    sqrtg_psi::Array{T,2}
    g_theta_theta::Array{T,2}
    g_theta_zeta::Array{T,2}
    g_zeta_zeta::Array{T,2}
end

function Metric(M::Int,N::Int; float_type::Type = Float64)
    sqrtg = zeros(float_type,M,N)
    sqrtg_psi = zeros(float_type,M,N)
    g_theta_theta = zeros(float_type,M,N)
    g_theta_zeta = zeros(float_type,M,N)
    g_zeta_zeta = zeros(float_type,M,N)
    return Metric{float_type}(sqrtg,sqrtg_psi,g_theta_theta,g_theta_zeta,g_zeta_zeta)#,g11,g12,g13,g22,g23,g33)
end

function Metric(coords::PestGrid{T}, inv_map::InverseMap{T}) where {T}
    M, N = (coords.M_theta, coords.N_zeta)
    (M,N) == size(inv_map.R) || throw(DimensionMismatch("$(coords) and $(inv_map) have incompatible sizes!"))
    sqrtg = Array{T}(undef,M,N)
    sqrtg_psi = Array{T}(undef,M,N)
    g_theta_theta = @. inv_map.R_theta^2 + inv_map.Z_theta^2
    g_zeta_zeta = @. inv_map.R^2 + inv_map.R_zeta^2 + inv_map.Z_zeta^2
    g_theta_zeta = @. inv_map.R_theta * inv_map.R_zeta + inv_map.Z_theta * inv_map.Z_zeta
    return Metric{T}(sqrtg,sqrtg_psi,g_theta_theta,g_theta_zeta,g_zeta_zeta)
end


struct MagneticGeometry{T}
    B_mag::Array{T,2}
    grad_psi_mag::Array{T}
    curv_normal::Array{T,2}
    curv_geodesic::Array{T,2}
    torsion_normal::Array{T,2}
    local_shear::Array{T,2}
    lambda_PS::Array{T,2}
    D_coeff::Array{T,2}
    h_coeff::Array{T,2}
    B_mag_psi::Array{T,2}
end

function MagneticGeometry(M::Int,N::Int; float_type::Type = Float64)
    B_mag = zeros(float_type,M,N)
    grad_psi_mag = zeros(float_type,M,N)
    curv_normal = zeros(float_type,M,N)
    curv_geodesic = zeros(float_type,M,N)
    torsion_normal = zeros(float_type,M,N)
    local_shear = zeros(float_type,M,N)
    lambda_PS = zeros(float_type,M,N)
    D_coeff = zeros(float_type,M,N)
    h_coeff = zeros(float_type,M,N)
    B_mag_psi = zeros(float_type,M,N)
    return MagneticGeometry{float_type}(B_mag, grad_psi_mag, curv_normal, curv_geodesic, torsion_normal, local_shear, lambda_PS, D_coeff, h_coeff, B_mag_psi)
end


#Structure containing all the parameters to execute NE3DLE
mutable struct RuntimeParams
    M_theta::Int
    N_zeta::Int
    iota::Float64
    parallel_resolution::Int
    computed_field_periods::Float64
    field_line_label::Float64
    surface_quant::Dict{String,Float64}
    mapping::String
    debug_flag::Bool
    #proc_list::Array{Int,1}
    fl_coordinate::String
    nfp::Int
    RuntimeParams() = new()
    RuntimeParams(a,b,c,d,e,f,g,h,i,j,k) = new(a,b,c,d,e,f,g,h,i,j,k)
end

struct FieldLine{T}
    eta::Array{T,1}
    integrated_shear::Array{T,1}
    q_int_shear::Array{T,1}
    B_mag::Array{T,1}
    grad_psi_mag::Array{T,1}
    curv_normal::Array{T,1}
    curv_geodesic::Array{T,1}
    torsion_normal::Array{T,1}
    sqrtg::Array{T,1}
    local_shear::Array{T,1}
    B_mag_deriv::Array{T,1}
end

function FieldLine(params::RuntimeParams, coords::PestGrid{T}) where {T}
    #set up field line where zeta = (theta - alpha_0)/iota. 
    r_lim = (params.computed_field_periods*π/coords.N_field_periods*params.iota - params.field_line_label)/params.iota
    l_lim = (-params.computed_field_periods*π/coords.N_field_periods*params.iota - params.field_line_label)/params.iota
    de = abs((r_lim-l_lim))/params.parallel_resolution
    eta = sign(params.computed_field_periods)*(-abs(l_lim) .+ de .* range(0,stop=params.parallel_resolution-1))
    #Initialize the rest
    integrated_shear = zeros(T, params.parallel_resolution)
    q_int_shear = zeros(T, params.parallel_resolution)
    B_mag = zeros(T, params.parallel_resolution)
    grad_psi_mag = zeros(T, params.parallel_resolution)
    curv_normal = zeros(T, params.parallel_resolution)
    curv_geodesic = zeros(T, params.parallel_resolution)
    torsion_normal = zeros(T, params.parallel_resolution)
    sqrtg = zeros(T, params.parallel_resolution)
    local_shear = zeros(T, params.parallel_resolution)
    B_mag_deriv = zeros(T, params.parallel_resolution)
    return FieldLine{T}(eta, integrated_shear, q_int_shear, B_mag, grad_psi_mag, curv_normal, curv_geodesic, torsion_normal, sqrtg, local_shear, B_mag_deriv)
end


mutable struct OutputParams
    n_scan_runs::Int
    parameter_output_path::Vector{String}
    parameter_output_name::String
    scan_var::Dict{String,Any}
    function OutputParams()
        n_scan_runs = 1
        parameter_output_path = Vector{String}(undef,1)
        parameter_output_name = ""
        scan_var = Dict{String,Any}()
        return new(n_scan_runs, parameter_output_path, parameter_output_name, scan_var)
    end
end

struct HelicalAxisParams{T}
    R0::T
    ρ::T
    Δ::T
    N_turns::Int64
    κ::T
    δ::T
end

struct MillerParams{T}
    R0::T
    ρ::T
    δ::T
    κ::T
    drR0::T
    s_κ::T
    s_δ::T
    ζ::T
    s_ζ::T
end

struct NormalizingParams{T}
    R0::T
    ρ::T
    dpsi_drho::T
    Vp::T
    B_0::T
    mu_0::T
    L_ref::T
    B_ref::T
end

# Initialize normalizing values. For Miller case, dψ/dρ is not consistent with VMEC normalizations.
function NormalizingParams(par_dict::AbstractDict,shape::MillerParams,imap::InverseMap,coords::PestGrid)
    R0 = shape.R0
    Vp = 1.0
    B_0 = R0/Vp

    if haskey(par_dict,"a")
        L_ref = par_dict["a"]
    else
        L_ref = R0
    end
    function integrand(θ,x,drR0,s_κ,s_δ,ρ,R0,ζ,s_ζ)
        return (cos(x*sin(θ)-ζ*sin(2*θ)) + drR0*cos(θ+ζ*sin(2*θ))*(1+2*ζ*cos(2*θ)) + ((x*cos(θ)+s_κ*(1+x*cos(θ)))*sin(θ+ζ*sin(2*θ))+(s_ζ*(1+x*sin(θ)*sin(2*θ))-s_δ*sin(θ)*(1+2*ζ*cos(2*θ)))*cos(θ+ζ*sin(2*θ)))*sin(θ+x*sin(θ)))/(R0 + ρ*cos(θ + x*sin(θ)))
    end
    ρ = shape.ρ
    κ = shape.κ
    δ = shape.δ
    drR0 = shape.drR0
    s_κ = shape.s_κ
    s_δ = shape.s_δ
    ζ = shape.ζ
    s_ζ = shape.s_ζ

    x = asin(δ)
    γ = quadgk(θ->integrand(θ,x,drR0,s_κ,s_δ,ρ,R0,ζ,s_ζ),0,2*pi)[1]
    θ = 0
    #Because Bp is defined, we can find dpsi_drho instead of guessing a form.
    fft_plan = get_rfft_plan(coords)
    R_squared_normalization = real((fft_plan*(imap.R.^2))[1,1])
    if haskey(par_dict,"B_a")
        println("B_ref ignored for Miller equilibrium to match GENE 2.0 implementation") #B_ref = par_dict["B_a"]
    end
    B_ref = R_squared_normalization/R0
    dpsi_drho = γ*B_ref*ρ*κ/(2*π)*R0 #Note, F = L_ref*B_ref instead of R0*B_0
    
    return NormalizingParams(R0,ρ,dpsi_drho,Vp,B_0,4π*10^(-7),L_ref,B_ref)
end

# Assumed dψ/dρ = ρ B_0
function NormalizingParams(par_dict::AbstractDict,imap::InverseMap,coords::PestGrid)
    R0 = par_dict["R0"]
    if !isempty(keys(par_dict["Rmn"])) || !isempty(keys(par_dict["Zmn"]))
        R = imap.R
        Z = imap.Z
        M = coords.M_theta
        N = coords.N_zeta
        Nt = coords.N_field_periods
        ζ = transpose(coords.zeta).*ones(M,N)
        fft_plan = NE3DLE.get_rfft_plan(coords)
        Δ = 2*real(fft_plan*R)[1,end]
        R_prime = @. (R-R0)*cos(Nt*ζ) + Z*sin(Nt*ζ) - Δ + R0
        Z_prime = @. Z*cos(Nt*ζ) - (R-R0)*sin(Nt*ζ)
        Rmax = maximum(R_prime,dims=1)
        Rmin = minimum(R_prime,dims=1)
    
        ρ = sum((Rmax .- Rmin)./2)/(N)
    else
        ρ = par_dict["rho"]
    end
    Vp = 1.0
    B_0 = R0/Vp

    if haskey(par_dict,"a")
        L_ref = par_dict["a"]
    elseif haskey(par_dict,"s")
        L_ref = ρ/sqrt(par_dict["s"])
    else
        L_ref = R0
    end
    if haskey(par_dict,"B_a")
        B_ref = par_dict["B_a"]
    else
        B_ref = B_0
    end
    
    return NormalizingParams(R0,ρ,ρ*B_0,Vp,B_0,4π*10^(-7),L_ref,B_0)
end

struct GeneGeom{T}
    s0::T
    iota::T
    shat::T
    alpha_0::T
    major_R::T
    minor_a::T
    B_ref::T
    my_dpdx::T
    gridpoints::Int
    n_pol::Int
    nfp::Int
    Cy::T
    gxx::Vector{T}
    gxy::Vector{T}
    gyy::Vector{T}
    Bhat::Vector{T}
    jac::Vector{T}
    L2::Vector{T}
    L1::Vector{T}
    ∂B∂θ::Vector{T}
    θ::Vector{T}
end

function GeneGeom(params::RuntimeParams,fl_geom::FieldLine{T},norm::NormalizingParams{T}) where {T}
    L_ref = norm.L_ref
    R0 = norm.R0
    B_0 = norm.B_0
    ρ = norm.ρ
    minor_a = L_ref == R0 ? ρ : L_ref
    dpsi_drho = norm.dpsi_drho
    B_ref = norm.B_ref
    mu_0 = norm.mu_0
    iota = params.iota
    alpha_0 = params.field_line_label
    grad_p = params.surface_quant["grad_p"]
    grad_iota = params.surface_quant["grad_iota"]
    gridpoints = params.parallel_resolution
    nfp = params.nfp
    n_pol = convert(Int,ceil(params.computed_field_periods*iota/nfp))
    Cy = dpsi_drho*iota/B_ref/L_ref
    eta = fl_geom.eta 

    s0 = ρ^2/L_ref^2
    my_dpdx = -2*mu_0*dpsi_drho*L_ref*grad_p/(B_ref^2)
    shat = -(dpsi_drho*ρ)/iota*grad_iota

    gxx = fl_geom.grad_psi_mag.^2 /(dpsi_drho^2)
    gxy = -(fl_geom.B_mag .* fl_geom.integrated_shear .+ fl_geom.grad_psi_mag.^2 .*alpha_0.*grad_iota./iota) / (B_ref)
    gyy = (dpsi_drho/B_ref)^2 .* (fl_geom.B_mag.^2 .+ (fl_geom.integrated_shear.*fl_geom.B_mag.+ fl_geom.grad_psi_mag.^2 .*alpha_0.*grad_iota./iota).^2) ./ fl_geom.grad_psi_mag.^2
    Bhat = fl_geom.B_mag / abs(B_ref)
    jac = abs.(fl_geom.sqrtg * B_ref/(iota*L_ref))
    L2 = dpsi_drho/B_ref * L_ref * ((fl_geom.curv_normal .+ fl_geom.curv_geodesic.*fl_geom.integrated_shear) .* fl_geom.B_mag ./ fl_geom.grad_psi_mag .- mu_0*grad_p./fl_geom.B_mag .+ grad_iota./iota.*alpha_0.*fl_geom.curv_geodesic.*fl_geom.grad_psi_mag)
    #Extra minus sign on L1 needed
    L1 = L_ref / (dpsi_drho) * fl_geom.curv_geodesic .* fl_geom.grad_psi_mag
    ∂B∂θ = fl_geom.B_mag_deriv ./ (B_ref*iota)
    θ = params.fl_coordinate == "zeta" ?  eta/pi : (alpha_0 .+ eta*iota)/(pi)
    #The GIST output is α_GIST = -α_0/ι for some reason
    return GeneGeom{T}(s0,iota,shat,-alpha_0/iota,R0,minor_a,B_ref,my_dpdx,gridpoints,n_pol,nfp,Cy,gxx,gxy,gyy,Bhat,jac,L2,L1,∂B∂θ,θ)
end

struct NE3DLESurface{T} <: PlasmaEquilibriumToolkit.AbstractMagneticEquilibrium
    coords::PestGrid{T}
    params::RuntimeParams
    norms::NormalizingParams{T}
    imap::InverseMap{T}
    metric::Metric{T}
    geom::MagneticGeometry{T}
end

function NE3DLESurface(surface::NE3DLESurface)
    return surface
end

struct NE3DLEFieldLine{T} <: PlasmaEquilibriumToolkit.AbstractMagneticEquilibrium
    params::RuntimeParams
    norms::NormalizingParams{T}
    fl::FieldLine{T}
    gene::GeneGeom{T}
end

function data_type(::NE3DLESurface{T}) where {T}
    return T
end

function data_type(::NE3DLEFieldLine{T}) where {T}
    return T
end

types_dict = Dict{String,Type}()
types_dict["N_field_periods"] = Int64
types_dict["iota"] = Float64
types_dict["alpha_param"] = Float64
types_dict["grad_p"] = Float64
types_dict["s_param"] = Float64
types_dict["grad_iota"] = Float64
types_dict["sigma"] = Float64
types_dict["nz0"] = Int64
types_dict["computed_field_periods"] = Float64
types_dict["n_pol"] = Int64
types_dict["alpha_0"] = Float64
types_dict["M_theta"] = Int64
types_dict["N_zeta"] = Int64
types_dict["mapping"] = String
types_dict["fl_coordinate"] = String

types_dict["Del"] = Float64
types_dict["R0"] = Float64
types_dict["rho"] = Float64
types_dict["a"] = Float64
types_dict["B_a"] = Float64
types_dict["kappa"] = Float64
types_dict["delta"] = Float64
types_dict["s_kappa"] = Float64
types_dict["s_delta"] = Float64
types_dict["drR0"] = Float64
types_dict["zeta"] = Float64
types_dict["s_zeta"] = Float64

types_dict["s"] = Float64
types_dict["vmec_file"] = String

types_dict["out_path"] = String
types_dict["out_name"] = String

types_dict["Rmn"] = Array{Float64,2}
types_dict["Zmn"] = Array{Float64,2}