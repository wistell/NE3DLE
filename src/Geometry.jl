"""
    Calculates ∇p, ∇ι, or σ, if not specified.
    Allows for direct specification or from dimensionless quanities
        α (alpha_param) and shat (s_param).
    Errors if:
    Too few surface quantites specified
    Too many surface quantities specified
    α and ∇p specified
    s_param and ∇ι specified
"""

function compute_input_surface_quantitites(par_dict::AbstractDict,
                                           norms::NormalizingParams{T};
                                          ) where {T}
    iota = par_dict["iota"]
    
    surface_quant_check = ["alpha_param","s_param","sigma","grad_iota","grad_p"]
    num_surface_quantities_given = 0

    for surfQuant in surface_quant_check
        if haskey(par_dict,surfQuant)
            num_surface_quantities_given += 1
        end
    end

    num_surface_quantities_given > 1 || error("Too few surface quantities specified.")
    num_surface_quantities_given < 3 || error("Too many surface quantities specified.")

    if haskey(par_dict,"alpha_param") && haskey(par_dict,"grad_p")
        error("Specify either alpha_param or grad_p.")
    elseif haskey(par_dict,"s_param") && haskey(par_dict,"grad_iota")
        error("Specify either s_param or grad_iota.")
    elseif !haskey(par_dict,"sigma")
        if haskey(par_dict, "s_param")
            grad_iota = par_dict["s_param"]*iota/(-norms.dpsi_drho*norms.ρ)
        elseif haskey(par_dict, "grad_iota")
            grad_iota = par_dict["grad_iota"]
        end
        if haskey(par_dict, "alpha_param")
            grad_p = -par_dict["alpha_param"]*(iota^2)*norms.B_ref^2/(2*norms.mu_0*norms.dpsi_drho*norms.L_ref)
        elseif haskey(par_dict, "grad_p")
            grad_p = par_dict["grad_p"]
        end
        surface_quant = Dict{String,Float64}("grad_p"=>grad_p,"grad_iota"=>grad_iota)
    elseif !haskey(par_dict,"s_param") && !haskey(par_dict,"grad_iota")
        sigma = par_dict["sigma"]
        if haskey(par_dict, "alpha_param")
            grad_p = -par_dict["alpha_param"]*(iota^2)*norms.B_ref^2/(2*norms.mu_0*norms.dpsi_drho*norms.L_ref)
        elseif haskey(par_dict, "grad_p")
            grad_p = par_dict["grad_p"]
        end
        surface_quant = Dict{String,Float64}("grad_p"=>grad_p,"surface_current"=>sigma)
    elseif !haskey(par_dict,"alpha_param") && !haskey(par_dict,"grad_p")
        sigma = par_dict["sigma"]
        if haskey(par_dict, "s_param")
            grad_iota = par_dict["s_param"]*iota/(-norms.dpsi_drho*norms.ρ)
        elseif haskey(par_dict, "grad_iota")
            grad_iota = par_dict["grad_iota"]
        end
        surface_quant = Dict{String,Float64}("grad_iota"=>grad_iota,"surface_current"=>sigma)
    end
   return surface_quant
end

function compute_geometry(params::RuntimeParams,
                          coords::PestGrid{T},
                          imap::InverseMap{T},
                          metric::Metric{T},
                          norm::NormalizingParams{T}
                         ) where {T}
    M = coords.M_theta
    N = coords.N_zeta
    M_pol = div(M,2)
    N_tor = div(N,2)
    geom = MagneticGeometry(M,N)
    iota = params.iota
    mu_0 = norm.mu_0

    fft_theta_coeffs = iseven(M) ? vcat(rfftfreq(M-1, M-1),[0.0+im*0.0]) : rfftfreq(M, M)
    fft_zeta_coeffs = iseven(N) ? coords.N_field_periods * transpose(fftfreq(N, N).*(abs.(fftfreq(N, N)).<div(N,2))) : coords.N_field_periods * transpose(fftfreq(N,N))

    B_star = @. sqrt(metric.g_zeta_zeta + 2*iota*metric.g_theta_zeta + iota^2*metric.g_theta_theta)
    Psi_star = @. sqrt(metric.g_zeta_zeta*metric.g_theta_theta - metric.g_theta_zeta^2)
    geom.B_mag .= B_star./metric.sqrtg
    geom.grad_psi_mag .= Psi_star./metric.sqrtg
    geom.curv_normal .= @. 1.0/(B_star^2 *Psi_star) * (
                           imap.R*(imap.Z_theta*(imap.R_zeta_zeta-imap.R)-imap.R_theta*imap.Z_zeta_zeta) - 2*(imap.Z_theta*imap.R_zeta-imap.Z_zeta*imap.R_theta)*imap.R_zeta +
                           2*iota*(imap.R*(imap.Z_theta*imap.R_theta_zeta-imap.Z_theta_zeta*imap.R_theta) - imap.R_theta*(imap.Z_theta*imap.R_zeta-imap.Z_zeta*imap.R_theta)) +
                           iota^2*imap.R*(imap.Z_theta*imap.R_theta_theta-imap.R_theta*imap.Z_theta_theta))

    geom.curv_geodesic .= @. 1.0/(B_star^3*Psi_star) * (
                             (imap.R_zeta_zeta + 2*iota*imap.R_theta_zeta + iota^2*imap.R_theta_theta - imap.R) * ((metric.g_zeta_zeta+iota*metric.g_theta_zeta)*imap.R_theta - (metric.g_theta_zeta+iota*metric.g_theta_theta)*imap.R_zeta) -
                             2*imap.R*(imap.R_zeta+iota*imap.R_theta)*(metric.g_theta_zeta+iota*metric.g_theta_theta) + (imap.Z_zeta_zeta+2*iota*imap.Z_theta_zeta+iota^2*imap.Z_theta_theta) *
                             ((metric.g_zeta_zeta+iota*metric.g_theta_zeta)*imap.Z_theta - (metric.g_theta_zeta+iota*metric.g_theta_theta)*imap.Z_zeta))

    geom.torsion_normal .= @. 1.0/(B_star^2*Psi_star^2) * (
                              (metric.g_zeta_zeta+iota*metric.g_theta_zeta)*(imap.R*imap.R_theta*(imap.Z_theta_zeta+iota*imap.Z_theta_theta) - imap.R_theta*(imap.Z_theta*imap.R_zeta-imap.Z_zeta*imap.R_theta) - imap.R*imap.Z_theta*(imap.R_theta_zeta+iota*imap.R_theta_theta)) +
                              (metric.g_theta_zeta+iota*metric.g_theta_theta)*(imap.R*imap.Z_theta*(imap.R_zeta_zeta+iota*imap.R_theta_zeta-imap.R)- imap.R*imap.R_theta*(imap.Z_zeta_zeta+iota*imap.Z_theta_zeta) -
                              (imap.Z_theta*imap.R_zeta-imap.Z_zeta*imap.R_theta)*(2*imap.R_zeta+iota*imap.R_theta)))

    lambda_temp_array = @. 2*metric.sqrtg * geom.curv_geodesic * Psi_star/B_star
    sigma_coeff_array = @. B_star^2 /Psi_star^2
    torsion_coeff_array = @. B_star^2 *geom.torsion_normal/Psi_star^2
    JB2_coeff_array = @. geom.B_mag^2 *metric.sqrtg
    # Compute the Pfirsch-Schluter lambda function. See documentation for
    # details
    fft_plan = get_rfft_plan(coords)

    lambda_PS_array = fft_plan*lambda_temp_array
    JB2_fft_array =  fft_plan*JB2_coeff_array
    integration_modes = @. -im/(iota * fft_theta_coeffs + fft_zeta_coeffs) * (iota * fft_theta_coeffs + fft_zeta_coeffs != 0)
    #With a Nyquist mode, an integration or derivative generates A_N/2 sin(pi) = 0
    if iseven(M) && iseven(N)
        integration_modes[div(M,2)+1,div(N,2)+1] = 0.0+im*0.0
    end
    lambda_fft_array = integration_modes .* lambda_PS_array

    #Compute lambda_coef[0,0] using surface average of <|B|^2*lambda> = 0.
    #lambda_coef[0,0] = -1/(J*|B|^2)[0,0] ∑_m ∑_n lambda_coef[m,n]*(J*|B|^2)[-m,-n]

    lambda_fft_array[1,1] = 0.0 + im*0.0

    #Use rfft and reality condition to get sums
    lambda_00_array = -conj.(JB2_fft_array).*lambda_fft_array./JB2_fft_array[1,1]
    lambda_fft_array[1,1] = iseven(M) ? sum(lambda_00_array[1,2:end]) + sum(lambda_00_array[2:end,:])+sum(conj.(lambda_00_array[2:end-1,:])) : sum(lambda_00_array[1,2:end]) + sum(lambda_00_array[2:end,:].+conj.(lambda_00_array[2:end,:]))

    geom.lambda_PS .= (fft_plan \ lambda_fft_array) * mu_0
    # Compute the relation between σ, ι', and p'

    lambda_coeff = flux_surface_avg(coords,metric,geom.lambda_PS .* sigma_coeff_array)
    sigma_coeff = flux_surface_avg(coords,metric,sigma_coeff_array)
    torsion_coeff = flux_surface_avg(coords,metric,torsion_coeff_array)

    if !haskey(params.surface_quant,"surface_current")
        surface_current = (params.surface_quant["grad_iota"] -
                           params.surface_quant["grad_p"]*lambda_coeff + 2*torsion_coeff) / sigma_coeff
        params.surface_quant["surface_current"] = surface_current
        grad_p = params.surface_quant["grad_p"]
        grad_iota = params.surface_quant["grad_iota"]
    elseif !haskey(params.surface_quant,"grad_iota")
        grad_iota = params.surface_quant["surface_current"]*sigma_coeff +
                    params.surface_quant["grad_p"]*lambda_coeff - 2*torsion_coeff
        params.surface_quant["grad_iota"] = grad_iota
        grad_p = params.surface_quant["grad_p"]
        surface_current = params.surface_quant["surface_current"]
    elseif !haskey(params.surface_quant,"grad_p")
        grad_p = (params.surface_quant["grad_iota"] -
                  params.surface_quant["surface_current"]*sigma_coeff + 2*torsion_coeff) / lambda_coeff
        params.surface_quant["grad_p"] = grad_p
        grad_iota = params.surface_quant["grad_iota"]
        surface_current = params.surface_quant["surface_current"]
    else
        grad_p = params.surface_quant["grad_p"]
        grad_iota = params.surface_quant["grad_iota"]
        surface_current = params.surface_quant["surface_current"]
    end

    # Compute the local shear everywhere on the surface
    geom.local_shear .= @. surface_current + grad_p*geom.lambda_PS - 2*geom.torsion_normal

    # Compute radial derivatives of inverse mapping
    D_right_hand_side = @.B_star^2 / Psi_star^2 * (surface_current + grad_p*geom.lambda_PS - 2*geom.torsion_normal) - grad_iota/metric.sqrtg
    
    #surface_current*((geom.B_mag/geom.grad_psi_mag)^2 - sigma_coeff/metric.sqrtg) +
                        #grad_p*((geom.B_mag/geom.grad_psi_mag)^2 *geom.lambda_PS - lambda_coeff/metric.sqrtg) +
                        #2*(torsion_coeff/metric.sqrtg - (geom.B_mag/geom.grad_psi_mag)^2 *geom.torsion_normal)

    D_rhs_fft = fft_plan * (D_right_hand_side.*metric.sqrtg)
    D_coeff_fft = integration_modes .* D_rhs_fft
    #Need to think about the integration constant more - stellarator symmetric => D(0,0) = 0

    D_coeff_fft[1,1] = iseven(M) ? -sum(D_coeff_fft[1,2:end]) - sum(D_coeff_fft[2:end,:]) - sum(conj.(D_coeff_fft[2:end-1,:])) : -sum(D_coeff_fft[1,2:end]) - sum(D_coeff_fft[2:end,:].+conj.(D_coeff_fft[2:end,:]))
    geom.D_coeff .= (fft_plan \ D_coeff_fft)
    # Because dϕ/dψ = 0, from x', h can be computed
    geom.h_coeff .= @. (imap.Z_theta*imap.R_zeta-imap.Z_zeta*imap.R_theta)/(geom.grad_psi_mag^2 *imap.R) +
                    geom.D_coeff*(metric.g_theta_zeta+iota*metric.g_theta_theta)/(geom.B_mag^2 *metric.sqrtg)

    imap.R_psi .= @. imap.R*imap.Z_theta/(metric.sqrtg*geom.grad_psi_mag^2) +
                  geom.h_coeff*(imap.R_zeta+iota*imap.R_theta)/metric.sqrtg +
                  (imap.R_theta*(metric.g_zeta_zeta+iota*metric.g_theta_zeta)-imap.R_zeta*(metric.g_theta_zeta+iota*metric.g_theta_theta))*geom.D_coeff/(metric.sqrtg^2 *geom.B_mag^2)
    imap.Z_psi .= @. -imap.R*imap.R_theta/(metric.sqrtg*geom.grad_psi_mag^2) +
                  geom.h_coeff*(imap.Z_zeta+iota*imap.Z_theta)/metric.sqrtg +
                  (imap.Z_theta*(metric.g_zeta_zeta+iota*metric.g_theta_zeta)-imap.Z_zeta*(metric.g_theta_zeta+iota*metric.g_theta_theta))*geom.D_coeff/(metric.sqrtg^2 *geom.B_mag^2)

    compute_radial_derivatives!(coords,imap)

    sqrtg_B_dot_grad_Lambda_PS = fourier_derivative(coords,geom.lambda_PS,:dζ) .+ iota.*fourier_derivative(coords,geom.lambda_PS,:dθ)
    sqrtg_B_dot_grad_h = fourier_derivative(coords,geom.h_coeff,:dζ) .+ iota.*fourier_derivative(coords,geom.h_coeff,:dθ)
    #B_dot_∇_B = (fourier_derivative(coords,geom.B_mag,:dζ) .+ iota.*fourier_derivative(coords,geom.B_mag,:dθ))./metric.sqrtg

    metric.sqrtg_psi .= @. sqrtg_B_dot_grad_h - geom.D_coeff*sqrtg_B_dot_grad_Lambda_PS/norm.mu_0 + norm.mu_0*grad_p*metric.sqrtg/(geom.B_mag^2) + grad_iota*(metric.g_theta_zeta+iota*metric.g_theta_theta)/(geom.B_mag^2 *metric.sqrtg) - 2*metric.sqrtg*geom.curv_normal/geom.grad_psi_mag

    dB_star2dψ = @. (imap.R_zeta*imap.R_psi_zeta + imap.R*imap.R_psi + imap.Z_zeta*imap.Z_psi_zeta) + iota*(imap.R_psi_theta*imap.R_zeta + imap.R_theta*imap.R_psi_zeta + imap.Z_psi_theta*imap.Z_zeta + imap.Z_theta*imap.Z_psi_zeta) + iota^2 *(imap.R_theta*imap.R_psi_theta + imap.Z_theta*imap.Z_psi_theta) + grad_iota*(metric.g_theta_zeta + iota*metric.g_theta_theta)

    geom.B_mag_psi .= @. dB_star2dψ./(geom.B_mag*metric.sqrtg^2) - geom.B_mag*metric.sqrtg_psi/metric.sqrtg
    return geom
end
