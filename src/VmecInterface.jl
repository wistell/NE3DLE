using .VMEC

"""
    function initialize_from_eq!(par_dict::Dict, nc_file::AbstractString)
    function initialize_from_eq!(par_dict::Dict, vmec::Vmec)
    function initialize_from_eq!(par_dict::Dict, vmec_s::VmecSurface)

Function to load all relavent par_dict values to be used to compute a surface
from a VMEC equilibrium. The extracted `VmecSurface` is returned.
"""
function initialize_from_eq!(par_dict::Dict,
                             vmec_s::VmecSurface;
                            )
    par_dict["R0"] = vmec_s.Rmajor_p
    par_dict["a"] = vmec_s.Aminor_p
    par_dict["rho"] = par_dict["a"]*sqrt(par_dict["s"])
    par_dict["B_a"] = abs(vmec_s.phi[end])/(π*par_dict["a"]^2)
    par_dict["iota"] = vmec_s.chi[end]*vmec_s.phi[end]*vmec_s.iota[1]<0 ? vmec_s.iota[1] : -vmec_s.iota[1]
    par_dict["s_param"] = VMEC.shat(vmec_s)
    par_dict["grad_p"] = vmec_s.pres[2]*2/(par_dict["R0"]*par_dict["rho"]^2)
    par_dict["N_field_periods"] = vmec_s.nfp
end

function initialize_from_eq!(par_dict::Dict,
                             vmec::Vmec;
                            )
    s = par_dict["s"]
    vmec_s = VmecSurface(s, vmec);
    initialize_from_eq!(par_dict, vmec_s)
    return vmec_s
end

function initialize_from_eq!(par_dict::Dict,
                             nc_file::String;
                            )
    vmec = readVmecWout(nc_file)
    return initialize_from_eq!(par_dict, vmec)
end

function call_NE3DLE(surface::VmecSurface{T};
                     M_θ::Int=32,
                     N_ζ::Int=32,
                     α_0::T=0.0,
                     nz0::Int=128,
                     n_pol::Int=1,
                     fl_coordinate::String="theta",
                     Rmn::Dict{Tuple{Int,Int},T} = Dict{Tuple{Int,Int},T}(),
                     Zmn::Dict{Tuple{Int,Int},T} = Dict{Tuple{Int,Int},T}()
                    ) where {T}
    #NE3DLE uses right-handed coordinate system. This means in some cases the poloidal angle is opposite of VMEC: θ = sign_Bt_CW*θ
    sign_Bt_CW = -sign(surface.phi[1])   #Toroidal field clockwise from top view of cylindrical coordinate system?
    sign_Ip_CW = sign(surface.chi[1])    #Toroidal plasma current clockwise from top view of cylindrical coordinate system?
    theta_range = sign_Bt_CW*(0:2π/M_θ:2π-2π/M_θ)
    zeta_range = (0:2π/N_ζ:2π-2π/N_ζ)/surface.nfp
    flux_coords = MagneticCoordinateGrid(FluxCoordinates, surface.s*surface.phi[end]/(2π)*surface.signgs, theta_range, zeta_range);
    Cyl = CylindricalFromFlux()(flux_coords, surface);

    R0 = surface.Rmajor_p
    Vp = 1.0
    B_0 = R0/Vp
    a = surface.Aminor_p
    B_ref = abs(surface.phi[end])/(π*a^2)
    s0 = surface.s
    ρ = a*sqrt(s0)
    #The flux coordinates used in NE3DLE are always right handed with ζ = -ϕ.
    iota = sign_Ip_CW*abs.(surface.iota[1])
    shat = VMEC.shat(surface)
    grad_p = surface.pres[2]*2/(ρ^2*B_0)

    L_ref = a

    norms = NormalizingParams(R0, ρ, ρ*B_0*sign_Bt_CW, Vp, B_0, 4π*10^(-7), L_ref, B_0)
    grad_iota = -shat*iota/(norms.ρ*norms.dpsi_drho)
    surface_quant = Dict{String, T}("grad_p" => grad_p, "grad_iota" => grad_iota)
    params = RuntimeParams(M_θ, N_ζ, iota, nz0, n_pol*surface.nfp/iota, α_0, surface_quant, "VMEC", false, fl_coordinate, surface.nfp)

    R_map = Matrix(transpose(getfield.(Cyl, 1)))
    Z_map = Matrix(transpose(getfield.(Cyl, 3)))
    Phi_map = Matrix(transpose(getfield.(Cyl, 2)))

    coords = PestGrid(params.M_theta, params.N_zeta, params.nfp)
    imap = create_inverse_map(coords, R_map, Z_map, Phi_map)
    update_inverse_mapping_fourier_modes!(coords,imap,Rmn,Zmn)
    metric = Metric(coords, imap)

    H,  b,  indices = compute_convolution_arrays(params, coords, metric)
    compute_jacobian!(H, b, indices, coords, metric)

    if sum(metric.sqrtg .<= 0) != 0 
        println("Unphysical equilibrium: jacobian changes signs.")
        return nothing
    end

    geom = compute_geometry(params, coords, imap, metric, norms)

    return NE3DLESurface(coords, params, norms, imap, metric, geom)
end