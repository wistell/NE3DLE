# NE3DLE

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://jduff2.gitlab.io/NE3DLE/dev)
[![Build Status](https://gitlab.com/jduff2/NE3DLE/badges/master/pipeline.svg)](https://gitlab.com/jduff2/NE3DLE/pipelines)
[![Coverage](https://gitlab.com/jduff2/NE3DLE/badges/master/coverage.svg)](https://gitlab.com/jduff2/NE3DLE/commits/master)

Numerical Evaluation of 3D Local Equilibria (NE3DLE) generates solutions for a local three-dimensional (3D) ideal magnetohdyrodynamic (MHD) equilibrium. This model is the numerical implementation of the theory described in [C.C. Hegna, Phys. Plasmas, **7**, 3921 (2000)](https://doi.org/10.1063/1.1290282), and, in the user-defined case of an axisymmetric "Miller" equilibrium, [R.L. Miller, M.S. Chu, J.M. Greene, Y.R. Lin-Liu, and R.E. Waltz, **5**, 973 (1998)](https://doi.org/10.1063/1.872666).
## Introduction
The purpose of this program is use the shape of a constant flux surface of a confined plasma parameterized in $`R`$, $`\phi`$, $`Z`$ coordinates, and output important geometric quantities. Quantities on a two-dimensional mesh are in a straight field line coordinate system: $`\mathbf{B}=\nabla\psi\times\nabla(\theta-\iota\zeta)`$  where $`\psi`$ is the toroidal flux, $`\zeta=-\phi`$, and $` \iota`$ is the rotational transform. The user will choose the inverse mapping, rotational transform $`\iota`$, and a choice of two flux surface quantities: $`p'=\textrm{d} p/\textrm{d} \psi`$, $` \iota'=\textrm{d}  \iota/\textrm{d} \psi`$, or $`\sigma = \mu_0\langle\mathbf{J}\cdot\mathbf{B}\rangle/\langle B^2\rangle`$. Once quantities on the mesh are found, an unnormalized and normalized flux tube is constructed in PEST coordinates according to $`\alpha_0=\theta- \iota\zeta=\textrm{const.}`$, the field-line label. See loc3D_notes.tex for more details.
## Installation
Installation is easy. Using Julia's Pkg to download and install:
```
julia> using Pkg;
julia> Pkg.add(url="https://gitlab.com/jduff2/NE3DLE.git");
julia> using NE3DLE
```
## Input File
In the top directory of NE3DLE, the inputs are read from NE3DLE.in. Depending on the inverse mapping option and whether the user wants output files created, the necessary input variables may change.
### Universally Required Variables
```
- M_theta - number of poloidal grid points
- N_zeta - number of toroidal grid points
- nz0 - number of points in the field line computation
- alpha_0 - field line label consistent with the first paragraph. Note, alpha_0 = -iota*alpha_GIST, where alpha_GIST is the input to GIST
- computed_field_periods - number of field periods computed in field line **OR** n_pol - number of poloidal turns;

n_pol = computed_field_periods/N_field_periods * iota

- fl_coordinate - zeta or theta; printed in ninth column out output file
- mapping - type of inverse mapping to assume
```
### Almost Universally Required Variables - Needed for non-`vmec` mapping
```
- iota - rotational transform
- N_field_periods - number of field periods for configuration. e.g. miller requires 1
```
**AND** two of
```
- alpha_param - MHD normalized pressure gradient **OR** grad_P - pressure gradient
- s_param - global shear **OR** grad_iota - flux surface averaged local shear
- sigma - flux surface averaged parallel current
```
### Output Variables
```
- out_path - path to where output directory will be made
- out_name - name of output directory
```
### `mapping` Options
```
- helical_axis
    - R0 - Major radius
    - rho - minor radius of flux surface
    - Del - minor radius variation in zeta
    - kappa - elongation
    - delta - triangularity
- miller
    - R0 - Major radius
    - rho - minor radius of flux surface
    - delta - triangularity
    - kappa - elongation
    - zeta - squareness
    - drR0 - radial shift
    - s_kappa - normalized radial derivative of kappa. May be chosen, otherwise computed as (kappa - 1)/kappa
    - s_delta - normalized radial derivative of delta. May be chosen, otherwise computed as delta/(1-delta^2)^(0.5)
    - s_zeta - normalized radial derivative of zeta. May be chosen, otherwise computed as zeta
- vmec
    - s - flux surface label in normalized toroidal flux
    - vmec_file - full path to wout.nc file containing VMEC output
- Optional normalizing parameters.
    - a - Device minor radius
    - B_a - Edge magnetic field strength
    NOTE: If not specified, Lref = R0.
          If minor radius is specified, Lref = a.
                For Bref to be written in the output file, B_a must be specified.
```
## Use
NE3DLE can be used by using the input text file, and input parameter dictionary, or by supplying a VmecSurface via the [PlasmaEquilibriumToolkit.jl](https://gitlab.com/wistell/PlasmaEquilibriumToolkit.jl) and [VMEC.jl](https://gitlab.com/wistell/VMEC.jl) packages.
### Input text file NE3DLE.in
To output or store all quantities of importance, use `call_NE3DLE("\path\to\NE3DLE.in")`:
```
julia> using NE3DLE;
julia> surface = call_NE3DLE("\path\to\NE3DLE.in")
julia> field_line = NE3DLEFieldLine(surface)
```

To automatically create and output directory and output files, which include the NE3DLE.in file and a GIST-like file, in the top directory of NE3DLE
```
julia> using NE3DLE;
julia> NE3DLE_exec()
```
### VmecSurface
A VmecSurface can be generated, then passed to the `call_NE3DLE` function:
```
julia> using VMEC, NE3DLE;
julia> vmec,vmec_data = readVmecWout("path\to\vmec_wout.nc"); s = 0.5; vmec_s = VmecSurface(s,vmec);
julia> surface = call_NE3DLE(vmec_s)
```
Optional keyword arguments include real space resolution `M_θ` and `N_ζ`, the field line label `α_0`, the real space field line resolution `nz0`, and the number of poloidal turns `n_pol`.
### Update NE3DLESurface
If one wants to modify an existing NE3DLEsurface, after modifying the desired quantity in the parameter dictionary `par_dict`, run:
```
julia> surface1  = call_NE3DLE(par_dict)
```
where `par_dict` is obtained from reading the input file, and optionally adding fixed values of Fourier harmonics for 
the inverse mapping coordinates.
## Optimal Settings
Numerical settings in NE3DLE affects the fidelity of the solution. To asses the fidelity of the Jacobian calculation, the residuals of the zero normal current condition may be computed by
```
julia> residual = NE3DLE.check_jacobian(surface.params,surface.metric,surface.coords)
```
In addition to grid resolution, the geometry of the flux surface affects the residuals. Low-aspect-ratio surfaces ($`A=R_0/\rho<5`$) see larger residuals than high-aspect-ratio surfaces. Also, if a surface has high spectral content, it may be more expensive to reduce the residuals.

Currently, a highly-shaped 3D surface ($`A=9.5`$) on $`\texttt{M\_theta}\times\texttt{N\_zeta}=64\times 128`$ has $`||\tau||_\infty=1.26\times10^{-10}`$, compared to the VMEC solution with $`||\tau||_\infty=10^{-2}`$, where $`\tau`$ is the matrix of residuals at each gridpoint on the surface. About 1.09 GB of memory is used, with about 2s of run time.

## Use with StellaratorOptimization.jl
A local equilibrium can be optimized for targets in the \src\optimization\OptimizationTargets.jl file by using the [StellaratorOptimization.jl](https://gitlab.com/wistell/StellaratorOptimization.jl) package. Method extensions to StellaratorOptimization.jl for NE3DLE can be found in the \src\optimization\StellOptInterface.jl file. In addition to the input parameters above, perturbations to the Fourier modes of the inverse mapping used to analytically describe the local helical axis case. The perturbations to the Fourier modes are dictionaries of the mode number tuples, e.g. `(1,0)`, and the Fouier mode amplitude. The dictionaries are named `Rmn` and `Zmn` and cannot be read in from an input file. The input parameters dictionary must be manually changed. The StellaratorOptimization.jl framework can edit the input parameters dictionary when the Fourier perturbations are used as optimization variables.

## Known Issues
- If the Jacobian changes sign, call_NE3DLE returns `nothing`
- Computing the local MHD equilibrium solutions to a VMEC equilibrium flux surface inverse mapping will not be consistent with the VMEC equilibrium magnetic field and jacobian or quantities derived therefrom. 
- Replacing the jacobian in NE3DLE with VMEC gmn and B_mag with VMEC bmn is not enough to get NE3DLE to reproduce a VMEC surface. The likely cause is a difference in radial covariant vector due to VMEC using first-order finite differences, and NE3DLE using magentic differential equations for a local 3D equilibrium. Of note, the averaged parallel current is also computed to be substantially larger in NE3DLE than in VMEC.jl.
- Because Benchmarking with VMEC.jl and GIST for 3D equilibria is determined mostly by eye. If the field-line average of the quantities are very
similar and changing the field line label results in a similar shift, then the methods in NE3DLE are satisfactory.
- The orientation of poloidal and toroidal flux of a VMEC equlibrium flux surface can throw a sign in some places, and some more care is
required to address this.
- Benchmarking with HSX using VMEC.jl (see above point) confirms normalization in NE3DLE requires `B_0` as the normalizing field strength as a result of `Vp = R0/B_0 = 1 = sqrtg_0,0`. To keep Miller normalization `B_ref=B_0`, so the GIST header will not be consistent. 
- To get agreement with GENE's Miller equilibrium implementation, if using `a` in NE3DLE, set `minor_r=1`, `major_R=R0/a`, and `treps=R0/rho` in GENE 
