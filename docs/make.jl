using NE3DLE
using Documenter

DocMeta.setdocmeta!(NE3DLE, :DocTestSetup, :(using NE3DLE); recursive=true)

makedocs(;
    modules=[NE3DLE],
    authors="Joey Duff <jduff2@wisc.edu> and contributors",
    repo="https://gitlab.com/jduff2/NE3DLE/blob/{commit}{path}#{line}",
    sitename="NE3DLE.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://jduff2.gitlab.io/NE3DLE",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
