(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 12.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     25654,        711]
NotebookOptionsPosition[     23394,        669]
NotebookOutlinePosition[     23729,        684]
CellTagsIndexPosition[     23686,        681]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"R", "[", 
    RowBox[{"\[Theta]_", ",", "\[Zeta]_"}], "]"}], ":=", 
   RowBox[{"R0", "+", 
    RowBox[{"\[Rho]", " ", 
     RowBox[{"Cos", "[", 
      RowBox[{"\[CapitalXi]", " ", "\[Theta]"}], "]"}]}], "+", 
    RowBox[{"\[CapitalDelta]", " ", 
     RowBox[{"Cos", "[", 
      RowBox[{"\[CapitalLambda]", " ", "\[Zeta]"}], "]"}]}]}]}], 
  ";"}]], "Input",
 CellChangeTimes->{{3.783181472682959*^9, 3.783181513902665*^9}, {
  3.7831825459093533`*^9, 3.7831825473711767`*^9}, {3.783182881339755*^9, 
  3.783182891945859*^9}},
 CellLabel->"In[1]:=",ExpressionUUID->"4f441b08-9f46-40f9-8f3b-1840727f5eeb"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"Z", "[", 
    RowBox[{"\[Theta]_", ",", "\[Zeta]_"}], "]"}], ":=", 
   RowBox[{
    RowBox[{"\[Rho]", " ", 
     RowBox[{"Sin", "[", 
      RowBox[{"\[CapitalXi]", " ", "\[Theta]"}], "]"}]}], "+", 
    RowBox[{"\[CapitalDelta]", " ", 
     RowBox[{"Sin", "[", 
      RowBox[{"\[CapitalLambda]", " ", "\[Zeta]"}], "]"}]}]}]}], 
  ";"}]], "Input",
 CellChangeTimes->{{3.783181503384581*^9, 3.783181535798518*^9}, {
  3.783182897230258*^9, 3.783182898650066*^9}},
 CellLabel->"In[2]:=",ExpressionUUID->"c06397a2-0fa8-4f6c-8ed9-8b49066c8af5"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"\[CapitalPhi]", "[", 
    RowBox[{"\[Theta]_", ",", "\[Zeta]_"}], "]"}], ":=", 
   RowBox[{"-", "\[Zeta]"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.7831815373237133`*^9, 3.783181561134231*^9}},
 CellLabel->"In[3]:=",ExpressionUUID->"1bffda16-5ae8-448d-b3e6-9b112c1f5ec6"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Symbolize", "[", 
   TemplateBox[{SubscriptBox["g", "\[Theta]\[Theta]"]},
    "NotationTemplateTag"], "]"}], ";", 
  RowBox[{"Symbolize", "[", 
   TemplateBox[{SubscriptBox["g", "\[Theta]\[Zeta]"]},
    "NotationTemplateTag"], "]"}], ";", 
  RowBox[{"Symbolize", "[", 
   TemplateBox[{SubscriptBox["g", "\[Zeta]\[Zeta]"]},
    "NotationTemplateTag"], "]"}], ";", 
  RowBox[{"Symbolize", "[", 
   TemplateBox[{SubscriptBox["R", "\[Theta]"]},
    "NotationTemplateTag"], "]"}], ";", 
  RowBox[{"Symbolize", "[", 
   TemplateBox[{SubscriptBox["R", "\[Zeta]"]},
    "NotationTemplateTag"], "]"}], ";", 
  RowBox[{"Symbolize", "[", 
   TemplateBox[{SubscriptBox["R", "\[Theta]\[Theta]"]},
    "NotationTemplateTag"], "]"}], ";", 
  RowBox[{"Symbolize", "[", 
   TemplateBox[{SubscriptBox["R", "\[Theta]\[Zeta]"]},
    "NotationTemplateTag"], "]"}], ";", 
  RowBox[{"Symbolize", "[", 
   TemplateBox[{SubscriptBox["R", "\[Zeta]\[Zeta]"]},
    "NotationTemplateTag"], "]"}], ";", 
  RowBox[{"Symbolize", "[", 
   TemplateBox[{SubscriptBox["Z", "\[Theta]"]},
    "NotationTemplateTag"], "]"}], ";", 
  RowBox[{"Symbolize", "[", 
   TemplateBox[{SubscriptBox["Z", "\[Zeta]"]},
    "NotationTemplateTag"], "]"}], ";", 
  RowBox[{"Symbolize", "[", 
   TemplateBox[{SubscriptBox["Z", "\[Theta]\[Theta]"]},
    "NotationTemplateTag"], "]"}], ";", 
  RowBox[{"Symbolize", "[", 
   TemplateBox[{SubscriptBox["Z", "\[Theta]\[Zeta]"]},
    "NotationTemplateTag"], "]"}], ";", 
  RowBox[{"Symbolize", "[", 
   TemplateBox[{SubscriptBox["Z", "\[Zeta]\[Zeta]"]},
    "NotationTemplateTag"], "]"}], ";"}]], "Input",
 CellChangeTimes->{{3.783181562831876*^9, 3.783181701558228*^9}, 
   3.783192982999452*^9},
 CellLabel->"In[16]:=",ExpressionUUID->"f97c5697-2880-4fee-a92c-32dc9b8556b2"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{
    SubscriptBox["R", "\[Theta]"], "[", 
    RowBox[{"\[Theta]_", ",", "\[Zeta]_"}], "]"}], ":=", 
   RowBox[{
    RowBox[{"D", "[", 
     RowBox[{
      RowBox[{"R", "[", 
       RowBox[{"x", ",", "y"}], "]"}], ",", "x"}], "]"}], "/.", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"x", "\[Rule]", "\[Theta]"}], ",", 
      RowBox[{"y", "\[Rule]", "\[Zeta]"}]}], "}"}]}]}], ";", 
  RowBox[{
   RowBox[{
    SubscriptBox["R", "\[Zeta]"], "[", 
    RowBox[{"\[Theta]_", ",", "\[Zeta]_"}], "]"}], ":=", 
   RowBox[{
    RowBox[{"D", "[", 
     RowBox[{
      RowBox[{"R", "[", 
       RowBox[{"x", ",", "y"}], "]"}], ",", "y"}], "]"}], "/.", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"x", "\[Rule]", "\[Theta]"}], ",", 
      RowBox[{"y", "\[Rule]", "\[Zeta]"}]}], "}"}]}]}], ";", 
  RowBox[{
   RowBox[{
    SubscriptBox["R", "\[Theta]\[Theta]"], "[", 
    RowBox[{"\[Theta]_", ",", "\[Zeta]_"}], "]"}], ":=", 
   RowBox[{
    RowBox[{"D", "[", 
     RowBox[{
      RowBox[{"D", "[", 
       RowBox[{
        RowBox[{"R", "[", 
         RowBox[{"x", ",", "y"}], "]"}], ",", "x"}], "]"}], ",", "x"}], "]"}],
     "/.", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"x", "\[Rule]", "\[Theta]"}], ",", 
      RowBox[{"y", "\[Rule]", "\[Zeta]"}]}], "}"}]}]}], ";", 
  RowBox[{
   RowBox[{
    SubscriptBox["R", "\[Theta]\[Zeta]"], "[", 
    RowBox[{"\[Theta]_", ",", "\[Zeta]_"}], "]"}], ":=", 
   RowBox[{
    RowBox[{"D", "[", 
     RowBox[{
      RowBox[{"D", "[", 
       RowBox[{
        RowBox[{"R", "[", 
         RowBox[{"x", ",", "y"}], "]"}], ",", "x"}], "]"}], ",", "y"}], "]"}],
     "/.", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"x", "\[Rule]", "\[Theta]"}], ",", 
      RowBox[{"y", "\[Rule]", "\[Zeta]"}]}], "}"}]}]}], ";", 
  RowBox[{
   RowBox[{
    SubscriptBox["R", "\[Zeta]\[Zeta]"], "[", 
    RowBox[{"\[Theta]_", ",", "\[Zeta]_"}], "]"}], ":=", 
   RowBox[{
    RowBox[{"D", "[", 
     RowBox[{
      RowBox[{"D", "[", 
       RowBox[{
        RowBox[{"R", "[", 
         RowBox[{"x", ",", "y"}], "]"}], ",", "y"}], "]"}], ",", "y"}], "]"}],
     "/.", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"x", "\[Rule]", "\[Theta]"}], ",", 
      RowBox[{"y", "\[Rule]", "\[Zeta]"}]}], "}"}]}]}], ";", 
  RowBox[{
   RowBox[{
    SubscriptBox["Z", "\[Theta]"], "[", 
    RowBox[{"\[Theta]_", ",", "\[Zeta]_"}], "]"}], ":=", 
   RowBox[{
    RowBox[{"D", "[", 
     RowBox[{
      RowBox[{"Z", "[", 
       RowBox[{"x", ",", "y"}], "]"}], ",", "x"}], "]"}], "/.", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"x", "\[Rule]", "\[Theta]"}], ",", 
      RowBox[{"y", "\[Rule]", "\[Zeta]"}]}], "}"}]}]}], ";", 
  RowBox[{
   RowBox[{
    SubscriptBox["Z", "\[Zeta]"], "[", 
    RowBox[{"\[Theta]_", ",", "\[Zeta]_"}], "]"}], ":=", 
   RowBox[{
    RowBox[{"D", "[", 
     RowBox[{
      RowBox[{"Z", "[", 
       RowBox[{"x", ",", "y"}], "]"}], ",", "y"}], "]"}], "/.", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"x", "\[Rule]", "\[Theta]"}], ",", 
      RowBox[{"y", "\[Rule]", "\[Zeta]"}]}], "}"}]}]}], ";", 
  RowBox[{
   RowBox[{
    SubscriptBox["Z", "\[Theta]\[Theta]"], "[", 
    RowBox[{"\[Theta]_", ",", "\[Zeta]_"}], "]"}], ":=", 
   RowBox[{
    RowBox[{"D", "[", 
     RowBox[{
      RowBox[{"D", "[", 
       RowBox[{
        RowBox[{"Z", "[", 
         RowBox[{"x", ",", "y"}], "]"}], ",", "x"}], "]"}], ",", "x"}], "]"}],
     "/.", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"x", "\[Rule]", "\[Theta]"}], ",", 
      RowBox[{"y", "\[Rule]", "\[Zeta]"}]}], "}"}]}]}], ";", 
  RowBox[{
   RowBox[{
    SubscriptBox["Z", "\[Theta]\[Zeta]"], "[", 
    RowBox[{"\[Theta]_", ",", "\[Zeta]_"}], "]"}], ":=", 
   RowBox[{
    RowBox[{"D", "[", 
     RowBox[{
      RowBox[{"D", "[", 
       RowBox[{
        RowBox[{"Z", "[", 
         RowBox[{"x", ",", "y"}], "]"}], ",", "x"}], "]"}], ",", "y"}], "]"}],
     "/.", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"x", "\[Rule]", "\[Theta]"}], ",", 
      RowBox[{"y", "\[Rule]", "\[Zeta]"}]}], "}"}]}]}], ";", 
  RowBox[{
   RowBox[{
    SubscriptBox["Z", "\[Zeta]\[Zeta]"], "[", 
    RowBox[{"\[Theta]_", ",", "\[Zeta]_"}], "]"}], ":=", 
   RowBox[{
    RowBox[{"D", "[", 
     RowBox[{
      RowBox[{"D", "[", 
       RowBox[{
        RowBox[{"Z", "[", 
         RowBox[{"x", ",", "y"}], "]"}], ",", "y"}], "]"}], ",", "y"}], "]"}],
     "/.", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"x", "\[Rule]", "\[Theta]"}], ",", 
      RowBox[{"y", "\[Rule]", "\[Zeta]"}]}], "}"}]}]}], ";", 
  RowBox[{
   RowBox[{
    SubscriptBox["g", "\[Theta]\[Theta]"], "[", 
    RowBox[{"x_", ",", "y_"}], "]"}], ":=", 
   RowBox[{
    SuperscriptBox[
     RowBox[{
      SubscriptBox["R", "\[Theta]"], "[", 
      RowBox[{"x", ",", "y"}], "]"}], "2"], "+", 
    SuperscriptBox[
     RowBox[{
      SubscriptBox["Z", "\[Theta]"], "[", 
      RowBox[{"x", ",", "y"}], "]"}], "2"]}]}], ";", 
  RowBox[{
   RowBox[{
    SubscriptBox["g", "\[Theta]\[Zeta]"], "[", 
    RowBox[{"x_", ",", "y_"}], "]"}], ":=", 
   RowBox[{
    RowBox[{
     RowBox[{
      SubscriptBox["R", "\[Theta]"], "[", 
      RowBox[{"x", ",", "y"}], "]"}], 
     RowBox[{
      SubscriptBox["R", "\[Zeta]"], "[", 
      RowBox[{"x", ",", "y"}], "]"}]}], "+", 
    RowBox[{
     RowBox[{
      SubscriptBox["Z", "\[Theta]"], "[", 
      RowBox[{"x", ",", "y"}], "]"}], 
     RowBox[{
      SubscriptBox["Z", "\[Zeta]"], "[", 
      RowBox[{"x", ",", "y"}], "]"}]}]}]}], ";", 
  RowBox[{
   RowBox[{
    SubscriptBox["g", "\[Zeta]\[Zeta]"], "[", 
    RowBox[{"x_", ",", "y_"}], "]"}], ":=", 
   RowBox[{
    SuperscriptBox[
     RowBox[{"R", "[", 
      RowBox[{"x", ",", "y"}], "]"}], "2"], "+", 
    SuperscriptBox[
     RowBox[{
      SubscriptBox["R", "\[Zeta]"], "[", 
      RowBox[{"x", ",", "y"}], "]"}], "2"], "+", 
    SuperscriptBox[
     RowBox[{
      SubscriptBox["Z", "\[Zeta]"], "[", 
      RowBox[{"x", ",", "y"}], "]"}], "2"]}]}], ";", 
  RowBox[{
   RowBox[{"F", "[", 
    RowBox[{"x_", ",", "y_"}], "]"}], ":=", 
   RowBox[{
    RowBox[{
     SubscriptBox["g", "\[Zeta]\[Zeta]"], "[", 
     RowBox[{"x", ",", "y"}], "]"}], "+", 
    RowBox[{"\[Iota]", " ", 
     RowBox[{
      SubscriptBox["g", "\[Theta]\[Zeta]"], "[", 
      RowBox[{"x", ",", "y"}], "]"}]}]}]}], ";", 
  RowBox[{
   RowBox[{"G", "[", 
    RowBox[{"x_", ",", "y_"}], "]"}], ":=", 
   RowBox[{
    RowBox[{
     SubscriptBox["g", "\[Theta]\[Zeta]"], "[", 
     RowBox[{"x", ",", "y"}], "]"}], "+", 
    RowBox[{"\[Iota]", " ", 
     RowBox[{
      SubscriptBox["g", "\[Theta]\[Theta]"], "[", 
      RowBox[{"x", ",", "y"}], "]"}]}]}]}], ";"}]], "Input",
 CellChangeTimes->{{3.783181727582902*^9, 3.7831821089334908`*^9}, 
   3.783193003720872*^9, 3.783792448121969*^9, {3.783794594297676*^9, 
   3.783794620893417*^9}},ExpressionUUID->"6bb49bee-1d9a-4f39-b2bc-\
78aaa9aac4f3"],

Cell[BoxData[
 RowBox[{
  RowBox[{"F", "[", 
   RowBox[{
    FractionBox["\[Pi]", "4"], ",", 
    FractionBox["\[Pi]", "4"]}], "]"}], "/.", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"R0", "\[Rule]", "1"}], ",", 
    RowBox[{"\[Rho]", "\[Rule]", "0.01"}], ",", 
    RowBox[{"\[CapitalDelta]", "\[Rule]", "0.01"}], ",", 
    RowBox[{"\[CapitalLambda]", "\[Rule]", "4"}]}], "}"}]}]], "Input",
 CellChangeTimes->{{3.783182126181645*^9, 3.7831821688807573`*^9}},
 CellLabel->"In[18]:=",ExpressionUUID->"27df1136-2e75-436f-bb41-3de90af51555"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Symbolize", "[", 
   TemplateBox[{SubscriptBox["M", "\[Theta]"]},
    "NotationTemplateTag"], "]"}], ";", 
  RowBox[{"Symbolize", "[", 
   TemplateBox[{SubscriptBox["N", "\[Zeta]"]},
    "NotationTemplateTag"], "]"}], ";"}]], "Input",
 CellChangeTimes->{{3.7831822080813007`*^9, 3.783182222133492*^9}},
 CellLabel->"In[7]:=",ExpressionUUID->"6f7a3164-8637-41be-8ef0-9465532078e7"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   SubscriptBox["M", "\[Theta]"], "=", "5"}], ";", 
  RowBox[{
   SubscriptBox["N", "\[Zeta]"], "=", "5"}], ";"}]], "Input",
 CellChangeTimes->{{3.783182242509444*^9, 3.783182251547868*^9}, {
  3.783182979283822*^9, 3.783182981937089*^9}, {3.783183072019178*^9, 
  3.783183074360989*^9}, {3.783186872930406*^9, 3.783186880855914*^9}, {
  3.78319266168519*^9, 3.7831926643917913`*^9}, {3.78379231871387*^9, 
  3.783792322325418*^9}},
 CellLabel->"In[21]:=",ExpressionUUID->"d80c0e90-85de-4361-92f7-879deec72662"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"MatrixForm", "[", 
   RowBox[{"Table", "[", 
    RowBox[{
     RowBox[{"F", "[", 
      RowBox[{
       RowBox[{
        FractionBox[
         RowBox[{"2", "\[Pi]"}], 
         RowBox[{"\[CapitalXi]", " ", 
          SubscriptBox["M", "\[Theta]"]}]], "i"}], ",", 
       RowBox[{
        FractionBox[
         RowBox[{"-", "\[Pi]"}], "\[CapitalLambda]"], "+", 
        RowBox[{
         FractionBox[
          RowBox[{"2", "\[Pi]"}], 
          RowBox[{"\[CapitalLambda]", " ", 
           SubscriptBox["N", "\[Zeta]"]}]], "j"}]}]}], "]"}], ",", 
     RowBox[{"{", 
      RowBox[{"i", ",", "0", ",", 
       RowBox[{
        SubscriptBox["M", "\[Theta]"], "-", "1"}]}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"j", ",", "0", ",", 
       RowBox[{
        SubscriptBox["N", "\[Zeta]"], "-", "1"}]}], "}"}]}], "]"}], "]"}], "/.", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"R0", "\[Rule]", "1"}], ",", 
    RowBox[{"\[Rho]", "\[Rule]", "0.01"}], ",", 
    RowBox[{"\[CapitalDelta]", "\[Rule]", "0.01"}], ",", 
    RowBox[{"\[CapitalXi]", "\[Rule]", "1"}], ",", 
    RowBox[{"\[CapitalLambda]", "\[Rule]", "100"}], ",", 
    RowBox[{"\[Iota]", "\[Rule]", "1"}]}], "}"}]}]], "Input",
 CellChangeTimes->{{3.7831821751648903`*^9, 3.78318234506779*^9}, {
  3.783182505358695*^9, 3.783182505540331*^9}, {3.783182589166568*^9, 
  3.7831825893477697`*^9}, {3.783182913107855*^9, 3.783182943121332*^9}, {
  3.783183052500593*^9, 3.7831830537055607`*^9}, {3.783792325927392*^9, 
  3.7837923308456984`*^9}},
 CellLabel->"In[22]:=",ExpressionUUID->"7eb41ba3-3881-41ba-811e-7dfe08c7f343"],

Cell[BoxData[
 TagBox[
  RowBox[{"(", "\[NoBreak]", GridBox[{
     {"1.99`", "2.0107772359201577`", "2.044597764079842`", 
      "2.044597764079842`", "2.0107772359201577`"},
     {"1.9831379156951559`", "1.99`", "2.0193955098312486`", 
      "2.0305758497187476`", "2.0080901699437494`"},
     {"1.9722370843048442`", "1.9746741502812528`", "1.99`", 
      "1.9969098300562504`", "1.9858544901687516`"},
     {"1.9722370843048442`", "1.9858544901687516`", "1.9969098300562504`", 
      "1.99`", "1.9746741502812528`"},
     {"1.9831379156951559`", "2.0080901699437494`", "2.0305758497187476`", 
      "2.0193955098312486`", "1.99`"}
    },
    GridBoxAlignment->{"Columns" -> {{Center}}, "Rows" -> {{Baseline}}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[0.7]}, 
        Offset[0.27999999999999997`]}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}}], "\[NoBreak]", ")"}],
  Function[BoxForm`e$, 
   MatrixForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{
  3.783182305630954*^9, {3.783182336381912*^9, 3.783182345447216*^9}, 
   3.783182505866405*^9, 3.783182555409048*^9, 3.78318258968731*^9, {
   3.783182928815331*^9, 3.783182943808848*^9}, 3.783183054283852*^9, {
   3.783792331606687*^9, 3.7837923369886503`*^9}},
 CellLabel->
  "Out[22]//MatrixForm=",ExpressionUUID->"bf95bf00-42f3-4d77-953c-\
33a22c660e45"]
}, Open  ]],

Cell[BoxData[
 RowBox[{
  RowBox[{"Fmat", " ", "=", " ", 
   RowBox[{
    RowBox[{"Table", "[", 
     RowBox[{
      RowBox[{"F", "[", 
       RowBox[{
        RowBox[{
         FractionBox[
          RowBox[{"2", "\[Pi]"}], 
          RowBox[{"\[CapitalXi]", " ", 
           SubscriptBox["M", "\[Theta]"]}]], "i"}], ",", 
        RowBox[{
         FractionBox[
          RowBox[{"2", "\[Pi]"}], 
          RowBox[{"\[CapitalLambda]", " ", 
           SubscriptBox["N", "\[Zeta]"]}]], "j"}]}], "]"}], ",", 
      RowBox[{"{", 
       RowBox[{"i", ",", "0", ",", 
        RowBox[{
         SubscriptBox["M", "\[Theta]"], "-", "1"}]}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"j", ",", "0", ",", 
        RowBox[{
         SubscriptBox["N", "\[Zeta]"], "-", "1"}]}], "}"}]}], "]"}], "/.", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"R0", "\[Rule]", "1"}], ",", 
      RowBox[{"\[Rho]", "\[Rule]", "0.01"}], ",", 
      RowBox[{"\[CapitalDelta]", "\[Rule]", "0.01"}], ",", 
      RowBox[{"\[CapitalXi]", "\[Rule]", "1"}], ",", 
      RowBox[{"\[CapitalLambda]", "\[Rule]", "100"}], ",", 
      RowBox[{"\[Iota]", "\[Rule]", "1"}]}], "}"}]}]}], ";"}]], "Input",
 CellChangeTimes->{{3.783182601843945*^9, 3.7831826205873528`*^9}, {
  3.78318296026*^9, 3.783182962546191*^9}, {3.783183057803316*^9, 
  3.783183059977379*^9}, {3.783183114283176*^9, 3.783183121272887*^9}, {
  3.7831831821629257`*^9, 3.783183191601015*^9}, {3.783186893171297*^9, 
  3.783186895096447*^9}, {3.783192700034605*^9, 3.78319270017612*^9}, {
  3.783792386800683*^9, 3.783792386958623*^9}},
 CellLabel->"In[28]:=",ExpressionUUID->"9b523535-49c2-4773-b8f4-64c5a287e258"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Gmat", " ", "=", " ", 
   RowBox[{
    RowBox[{"Table", "[", 
     RowBox[{
      RowBox[{"G", "[", 
       RowBox[{
        RowBox[{
         FractionBox[
          RowBox[{"2", "\[Pi]"}], 
          RowBox[{"\[CapitalXi]", " ", 
           SubscriptBox["M", "\[Theta]"]}]], "i"}], ",", 
        RowBox[{
         FractionBox[
          RowBox[{"2", "\[Pi]"}], 
          RowBox[{"\[CapitalLambda]", " ", 
           SubscriptBox["N", "\[Zeta]"]}]], "j"}]}], "]"}], ",", 
      RowBox[{"{", 
       RowBox[{"i", ",", "0", ",", 
        RowBox[{
         SubscriptBox["M", "\[Theta]"], "-", "1"}]}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"j", ",", "0", ",", 
        RowBox[{
         SubscriptBox["N", "\[Zeta]"], "-", "1"}]}], "}"}]}], "]"}], "/.", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"R0", "\[Rule]", "1"}], ",", 
      RowBox[{"\[Rho]", "\[Rule]", "0.01"}], ",", 
      RowBox[{"\[CapitalDelta]", "\[Rule]", "0.01"}], ",", 
      RowBox[{"\[CapitalXi]", "\[Rule]", "1"}], ",", 
      RowBox[{"\[CapitalLambda]", "\[Rule]", "100"}], ",", 
      RowBox[{"\[Iota]", "\[Rule]", "1"}]}], "}"}]}]}], ";"}]], "Input",
 CellChangeTimes->{{3.783192747464139*^9, 3.783192749703945*^9}, {
  3.783792392624803*^9, 3.7837923927748213`*^9}},
 CellLabel->"In[29]:=",ExpressionUUID->"cfb419ee-95d2-4fa9-8b8d-1cc2070e154a"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"MatrixForm", "[", 
  RowBox[{
   RowBox[{"Fourier", "[", "Fmat", "]"}], "/", "5"}], "]"}]], "Input",
 CellChangeTimes->{{3.783192868687335*^9, 3.783192877502565*^9}, {
  3.783792418016363*^9, 3.783792419869957*^9}},
 CellLabel->"In[30]:=",ExpressionUUID->"dd000597-43f3-4c40-aa89-4d295990d11a"],

Cell[BoxData[
 TagBox[
  RowBox[{"(", "\[NoBreak]", GridBox[{
     {"2.`", "8.201772594418344`*^-17", "9.56029213350381`*^-17", 
      "9.56029213350381`*^-17", "8.201772594418344`*^-17"},
     {"7.105427357601003`*^-17", 
      RowBox[{"-", "1.4710455076283328`*^-17"}], 
      RowBox[{"-", "3.7904536413757256`*^-17"}], "2.4810584203476552`*^-18", 
      "0.00500000000000001`"},
     {"8.881784197001253`*^-17", 
      RowBox[{"-", "4.3591553555053366`*^-17"}], "1.10882690157505`*^-17", 
      RowBox[{"-", "2.961823881655919`*^-17"}], 
      RowBox[{"-", "3.3072593113694826`*^-18"}]},
     {"8.881784197001253`*^-17", 
      RowBox[{"-", "3.3072593113694826`*^-18"}], 
      RowBox[{"-", "2.961823881655919`*^-17"}], "1.10882690157505`*^-17", 
      RowBox[{"-", "4.3591553555053366`*^-17"}]},
     {"7.105427357601003`*^-17", "0.00500000000000001`", 
      "2.4810584203476552`*^-18", 
      RowBox[{"-", "3.7904536413757256`*^-17"}], 
      RowBox[{"-", "1.4710455076283328`*^-17"}]}
    },
    GridBoxAlignment->{"Columns" -> {{Center}}, "Rows" -> {{Baseline}}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[0.7]}, 
        Offset[0.27999999999999997`]}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}}], "\[NoBreak]", ")"}],
  Function[BoxForm`e$, 
   MatrixForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{
  3.78319287864996*^9, {3.783792396405239*^9, 3.783792420356979*^9}, 
   3.7837924579012003`*^9},
 CellLabel->
  "Out[30]//MatrixForm=",ExpressionUUID->"e679a510-21dd-49ef-a2d8-\
da9bfa679ca8"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  SubscriptBox["g", "\[Theta]\[Zeta]"], "[", 
  RowBox[{"\[Theta]", ",", "\[Zeta]"}], "]"}]], "Input",
 CellChangeTimes->{{3.783792465767288*^9, 3.783792472853071*^9}},
 CellLabel->"In[31]:=",ExpressionUUID->"354e401d-b27b-4282-b913-2ebdc9172197"],

Cell[BoxData[
 RowBox[{
  RowBox[{
  "\[CapitalDelta]", " ", "\[CapitalLambda]", " ", "\[CapitalXi]", " ", 
   "\[Rho]", " ", 
   RowBox[{"Cos", "[", 
    RowBox[{"\[Zeta]", " ", "\[CapitalLambda]"}], "]"}], " ", 
   RowBox[{"Cos", "[", 
    RowBox[{"\[Theta]", " ", "\[CapitalXi]"}], "]"}]}], "+", 
  RowBox[{
  "\[CapitalDelta]", " ", "\[CapitalLambda]", " ", "\[CapitalXi]", " ", 
   "\[Rho]", " ", 
   RowBox[{"Sin", "[", 
    RowBox[{"\[Zeta]", " ", "\[CapitalLambda]"}], "]"}], " ", 
   RowBox[{"Sin", "[", 
    RowBox[{"\[Theta]", " ", "\[CapitalXi]"}], "]"}]}]}]], "Output",
 CellChangeTimes->{3.783792473209258*^9},
 CellLabel->"Out[31]=",ExpressionUUID->"c4bbf4de-417e-43d4-824e-a79f762f3608"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"MatrixForm", "[", 
  RowBox[{"Fourier", "[", "Gmat", "]"}], "]"}]], "Input",
 CellChangeTimes->{{3.7831928800154247`*^9, 3.783192884686555*^9}},
 CellLabel->"In[19]:=",ExpressionUUID->"fe28eb11-f15e-499e-942f-1d755f1b3f9d"],

Cell[BoxData[
 TagBox[
  RowBox[{"(", "\[NoBreak]", GridBox[{
     {"0.00030000000000000003`", "0.`", "0.`"},
     {"0.`", "3.979884562503598`*^-21", "0.00015000000000000001`"},
     {"0.`", "0.00015000000000000001`", "3.979884562503598`*^-21"}
    },
    GridBoxAlignment->{"Columns" -> {{Center}}, "Rows" -> {{Baseline}}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[0.7]}, 
        Offset[0.27999999999999997`]}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}}], "\[NoBreak]", ")"}],
  Function[BoxForm`e$, 
   MatrixForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{3.783192885217145*^9, 3.783193011772842*^9},
 CellLabel->
  "Out[19]//MatrixForm=",ExpressionUUID->"ab6e1628-2cff-4ab9-a4f6-\
dfc265e8434b"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"G", "[", 
   RowBox[{"0", ",", "0"}], "]"}], "/.", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"R0", "\[Rule]", "1"}], ",", 
    RowBox[{"\[Rho]", "\[Rule]", "0.01"}], ",", 
    RowBox[{"\[CapitalDelta]", "\[Rule]", "0.01"}], ",", 
    RowBox[{"\[CapitalXi]", "\[Rule]", "1"}], ",", 
    RowBox[{"\[CapitalLambda]", "\[Rule]", "1"}], ",", 
    RowBox[{"\[Iota]", "\[Rule]", "1"}]}], "}"}]}]], "Input",
 CellChangeTimes->{{3.783192911975062*^9, 3.7831929242565813`*^9}, {
  3.78319295426492*^9, 3.783192955374448*^9}},
 CellLabel->"In[15]:=",ExpressionUUID->"82fc0c42-d852-415f-927a-b3b0deffaa25"],

Cell[BoxData[
 RowBox[{"0.0001`", "\[VeryThinSpace]", "+", 
  RowBox[{
   SubscriptBox["\[Iota]g", "\[Theta]\[Theta]"], "[", 
   RowBox[{"0", ",", "0"}], "]"}]}]], "Output",
 CellChangeTimes->{3.7831929246095057`*^9, 3.783192956114092*^9},
 CellLabel->"Out[15]=",ExpressionUUID->"52463a1e-a269-43e9-8145-04ff85718a2e"]
}, Open  ]]
},
WindowSize->{1920, 1031},
WindowMargins->{{0, Automatic}, {24, Automatic}},
FrontEndVersion->"12.0 for Linux x86 (64-bit) (April 8, 2019)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 649, 16, 31, "Input",ExpressionUUID->"4f441b08-9f46-40f9-8f3b-1840727f5eeb"],
Cell[1210, 38, 585, 15, 31, "Input",ExpressionUUID->"c06397a2-0fa8-4f6c-8ed9-8b49066c8af5"],
Cell[1798, 55, 320, 7, 31, "Input",ExpressionUUID->"1bffda16-5ae8-448d-b3e6-9b112c1f5ec6"],
Cell[2121, 64, 1800, 43, 450, "Input",ExpressionUUID->"f97c5697-2880-4fee-a92c-32dc9b8556b2"],
Cell[3924, 109, 6855, 223, 364, "Input",ExpressionUUID->"6bb49bee-1d9a-4f39-b2bc-78aaa9aac4f3"],
Cell[10782, 334, 535, 13, 45, "Input",ExpressionUUID->"27df1136-2e75-436f-bb41-3de90af51555"],
Cell[11320, 349, 415, 9, 42, "Input",ExpressionUUID->"6f7a3164-8637-41be-8ef0-9465532078e7"],
Cell[11738, 360, 546, 11, 33, "Input",ExpressionUUID->"d80c0e90-85de-4361-92f7-879deec72662"],
Cell[CellGroupData[{
Cell[12309, 375, 1612, 41, 53, "Input",ExpressionUUID->"7eb41ba3-3881-41ba-811e-7dfe08c7f343"],
Cell[13924, 418, 1401, 31, 119, "Output",ExpressionUUID->"bf95bf00-42f3-4d77-953c-33a22c660e45"]
}, Open  ]],
Cell[15340, 452, 1651, 40, 53, "Input",ExpressionUUID->"9b523535-49c2-4773-b8f4-64c5a287e258"],
Cell[16994, 494, 1366, 36, 53, "Input",ExpressionUUID->"cfb419ee-95d2-4fa9-8b8d-1cc2070e154a"],
Cell[CellGroupData[{
Cell[18385, 534, 318, 6, 31, "Input",ExpressionUUID->"dd000597-43f3-4c40-aa89-4d295990d11a"],
Cell[18706, 542, 1593, 37, 134, "Output",ExpressionUUID->"e679a510-21dd-49ef-a2d8-da9bfa679ca8"]
}, Open  ]],
Cell[CellGroupData[{
Cell[20336, 584, 271, 5, 33, "Input",ExpressionUUID->"354e401d-b27b-4282-b913-2ebdc9172197"],
Cell[20610, 591, 703, 17, 35, "Output",ExpressionUUID->"c4bbf4de-417e-43d4-824e-a79f762f3608"]
}, Open  ]],
Cell[CellGroupData[{
Cell[21350, 613, 247, 4, 31, "Input",ExpressionUUID->"fe28eb11-f15e-499e-942f-1d755f1b3f9d"],
Cell[21600, 619, 792, 20, 89, "Output",ExpressionUUID->"ab6e1628-2cff-4ab9-a4f6-dfc265e8434b"]
}, Open  ]],
Cell[CellGroupData[{
Cell[22429, 644, 628, 14, 31, "Input",ExpressionUUID->"82fc0c42-d852-415f-927a-b3b0deffaa25"],
Cell[23060, 660, 318, 6, 35, "Output",ExpressionUUID->"52463a1e-a269-43e9-8145-04ff85718a2e"]
}, Open  ]]
}
]
*)

